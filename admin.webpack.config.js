
const path = require('path');
const fs = require('fs');
const glob  = require('glob');
const webpack = require('webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const autoprefixer = require('autoprefixer');

const LiveReloadPlugin = require('webpack-livereload-plugin');

const ENV = process.env.NODE_ENV;
const LOCAL = ENV === 'local';
const DEV = ENV === 'development';
const PROD = ENV === 'production';

const PATH = {
	src: path.resolve(__dirname, 'frontend\\admin'),
	dist: PROD ? path.resolve(__dirname, 'assets') : path.resolve(__dirname, 'assets')
};



let html = glob.sync(`${PATH.src}\\html\\*.pug`).reduce((x, y) => {
	let name = (/([-_\w]+).\w+$/gi).exec(y)[1],
		// for multiple entry point
		chunks
	;

	x.push(
		new HtmlWebpackPlugin({
				filename: `${name}.html`,
				template: `${PATH.src}\\html\\${name}.pug`,
				hash: true,
				inject: 'body'
			}
		))
	;

	return x;
}, []);



const config = {
	// for multiple entry point
	entry: {
		admin: ["@babel/polyfill", `${PATH.src}\\app.js`],
	},

	output: {
		path: PATH.dist,
		filename: 'js/[name].js'
	},

	resolve: {
		extensions: ['.js', '.vue', '.json'],
	},

	//stats: 'errors-only',

	stats: {
		colors: true,
		errorDetails: true,
	},

	mode: ENV === 'production' ? 'production' : 'development',

	devServer: {
		hot: true,
		contentBase: `${PATH.src}`,
		// contentBase: PATH.dist,
		// compress: true,
		// inline: true,
		open: true,
		// historyApiFallback: true
	},

	plugins: [
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery',
			'window.$': 'jquery'
		}),
		new webpack.DefinePlugin({
			'isLocal': JSON.stringify(LOCAL),
			'isDev': JSON.stringify(DEV),
			'isProd': JSON.stringify(PROD),
			'process.env': {
				NODE_ENV: JSON.stringify(ENV)
			}
		}),
		new MiniCssExtractPlugin({
			filename: function(e){
				return 'css/[name].css';
			},
			chunkFilename: '[id].css',
		}),
		new VueLoaderPlugin(),
		new CopyWebpackPlugin([

		]),
	]
		.concat(html  )

	,

	devtool: 'source-map',

	module:  {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader',
				options: {
					// postcss: [require('postcss-cssnext')()],
					// options: {
					//     extractCSS: true
					// },
					loaders: {
						// js: 'babel-loader'
					}
				}
			},
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: [{
					loader: "babel-loader",
					options: {
						presets: [[
							"@babel/preset-env",
							{
								targets: {
									browsers: [
										'Chrome >= 42',
										'Safari >= 10.1',
										'iOS >= 10.3',
										'Firefox >= 50',
										'Edge >= 12',
										'ie >= 10',
									],
								}
							}
						]],
						plugins: ['@vue/babel-plugin-transform-vue-jsx']
					}
				}]
			},
			{
				test: /\.pug$/,
				include: [`${PATH.src}\\html`],
				use:  [
					'html-loader',
					{
						loader: 'pug-html-loader',
						options: {
							pretty: true,
							exports: false,
						}
					}
				]
			},

			{
				test: /\.pug$/,
				exclude: [`${PATH.src}\\html`],
				oneOf: [
					// это применяется к `<template lang="pug">` в компонентах Vue
					{
						resourceQuery: /^\?vue/,
						use: {
							loader: 'pug-plain-loader',
							options: {
								pretty: true,
								exports: false
							}
						}
					},
					// это применяется к импортам pug внутри JavaScript
					{
						use: [
							{
								loader: 'raw-loader',
								options: {
									pretty: true,
									exports: false
								}
							},
							{
								loader: 'pug-plain-loader',
								options: {
									pretty: true,
									exports: false
								}
							}
						]
					}
				]
			},
			{
				test: /\.(gif|png|jpe?g|svg)$/i,
				// exclude: /(sprites|svg)/,
				include: [`${PATH.src}\\img`],
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							outputPath: '/',
							useRelativePath: true
						}
					},
					{
						loader: 'image-webpack-loader',
						options: {

						}
					}
				]
			},
			{
				test: /\.scss$/,
				exclude: /node_modules/,
				use: [
					LOCAL ?
					{loader: 'style-loader'} :
					{
						loader: MiniCssExtractPlugin.loader,
						options: {
							publicPath: '../',
							sourceMap: true
						}
					},
					{
						loader: 'css-loader',
						options: {
							sourceMap: LOCAL,
							minimize: true
						}
					},
					{
						loader: 'postcss-loader',
						options: {
							plugins: [
								autoprefixer({
									// browsers:['ie >= 8', 'last 4 version']
								})
							],
							name: LOCAL ? '[name][hash].[ext]' : '[name].[ext]',
							sourceMap: LOCAL,
							useRelativePath: true
						}
					},
					'resolve-url-loader',
					{
						loader: 'sass-loader',
						options: {
							sourceMap: LOCAL,
							data: `
								@import "frontend/admin/css/scss/_mixins.scss";
								@import "frontend/admin/css/scss/_variables.scss";
							`
						},
					}
				]
			},
			{
				test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
				include: [`${PATH.src}\\fonts`],
				use: [{
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						useRelativePath: true
					}
				}]
			}
		]
	},

	optimization: PROD ? {
		minimizer: [
			new UglifyJsPlugin({
				sourceMap: false,
				uglifyOptions: {
					//ecma: 5,
					output: {
						comments: false,
					},
					compress: {
						inline: true,
						warnings: false,
						drop_console: false,
						unsafe: true
					},
				},
			}),
			new OptimizeCSSAssetsPlugin({
				cssProcessorPluginOptions: {
					preset: ['default', { discardComments: { removeAll: true } }],
				},
			})
		]
	} : {}
};

module.exports = config;