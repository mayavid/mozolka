[
  {
   "caption": "Заголовок",
   "fields": [ 
                {
                  "field": "item-slide-text",
                  "caption": "Заголовок слайда",
                  "inputTVtype": "text",
                  "sourceFrom": "migx"
                 },  
               {
                "field": "item-slide-img",
                "caption": "Картинка",
                "inputTVtype": "image",
                "sourceFrom": "migx"
               },
               {
                  "field": "item-slide-alt-text",
                  "caption": "Alt text слайда",
                  "inputTVtype": "text",
                  "sourceFrom": "migx"
                 }

             ]
  }
]



[
  {
    "header": "Заголовок Слайда",
    "width": "100",
    "sortable": "true",
    "dataIndex": "item-slide-text"
  }, 
  {
    "header": "Image",
    "width": "100",
    "sortable": "false",
    "dataIndex": "item-slide-img",
    "renderer": "this.renderImage"
  },
  {
    "header": "Alt текст слайда",
    "width": "100",
    "sortable": "true",
    "dataIndex": "item-slide-alt-text"
  }
]