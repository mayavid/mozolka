{extends 'file:templates/layout.tpl'}

{block 'main'}
    <div class="website">
      <section>
        <div class="uk-container uk-container-large">
          <div class="uk-position-relative" tabindex="-1" uk-slideshow="animation: pull; autoplay: false; autoplay-interval: 6000; pause-on-hover: true; min-height: 300; max-height: 700">
            <ul class="uk-slideshow-items">
              <li><img src="/assets/img/banners/banner-1.jpg" alt="" uk-cover>
                <div class="uk-position-small uk-position-center mz-padding-left">
                  <p class="uk-width-1-1 uk-width-1-2@s uk-width-1-2@m  mz-h1 mz-main-clr">Здоровье Ваших ног - в руках надежных специалистов! </p>
                  <div><a class="mz-text-title mz-button-act" href="#modalexpl" uk-toggle>Записаться</a>
                    <svg class="uk-margin-medium-left mz-h-girl" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 93.43 107.87">
                      <use xlink:href="#my-icon" x="0" y="0"></use>
                    </svg>
                  </div>
                </div>
              </li>
              <li><img src="/assets/img/banners/banner-2.jpg" alt="" uk-cover></li>
              <li><img src="/assets/img/banners/banner-3.jpg" alt="" uk-cover></li>
            </ul><a class="uk-position-center-left uk-position-small uk-visible@s" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a><a class="uk-position-center-right uk-position-small uk-visible@s" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
          </div>
        </div>
        <div class="uk-container uk-container-small mz-bcg-dark-pattern uk-padding-large uk-position-relative uk-light mz-margin-negative-medium-top-l">
          <h2 class="mz-h2 uk-text-center">О нас</h2>
          <p class="mz-text">Аппаратный педикюр является абсолютно безопасной процедурой, исключающей риск заражения грибком и не вызывающей неприятных ощущений. При данной методике используют вместо ванночек химические размягчители, которые защищают от любых инфекций. С помощью сменных аппаратных насадок (одноразовых или стерилизованных) кожа на ножках обрабатывается мягко и деликатно даже в труднодоступных местах, что особенно важно для проблемных ног.</p>
          <p class="mz-text">Аппаратный педикюр – процедура не только гигиеническая, но и лечебная. С ее помощью решаются такие проблемы, как гиперкератоз (чрезмерное утолщение рогового слоя эпидермиса), вросший ноготь, гипергидроз (повышенная потливость стоп), микоз (грибковое заболевание), подошвенные бородавки, твердые мозоли и многие другие. Аппаратный методика показана для больных диабетом (при снижении болевой и температурной чувствительности, характерной для этой болезни, можно пораниться, делая классический маникюр).</p>
          <p class="mz-text">Специалисты, выполняющие аппаратный педикюр, могут не только провести тщательную обработку стоп и ногтей, диагностировать заболевания даже на ранних стадиях, но и рекомендовать способы домашнего ухода.</p>
        </div>
      </section>
      <section class="uk-section uk-section-small">
        <div class="uk-container">
          <h2 class="mz-h2 uk-text-center uk-padding">Наши услуги</h2>
          <div class="uk-text-center uk-grid uk-grid-match uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m uk-child-width-1-4@l uk-grid-small uk-grid-medium@m" uk-grid uk-height-match="target: &gt; div &gt; div">
            <div><img class="uk-border-rounded" src="/assets/img/services/servises_1.jpg">
              <div class="uk-padding">Медицинский педикюр</div>
            </div>
            <div><img class="uk-border-rounded" src="/assets/img/services/servises_1.jpg">
              <div class="uk-padding">Обработка ногтей и подошвы, пораженных грибком</div>
            </div>
            <div><img class="uk-border-rounded" src="/assets/img/services/servises_1.jpg">
              <div class="uk-padding">Лечение пораженной ногтевой пластины (гиперкератоз)</div>
            </div>
            <div><img class="uk-border-rounded" src="/assets/img/services/servises_1.jpg">
              <div class="uk-padding">Коррекция вросшего ногтя</div>
            </div>
            <div><img class="uk-border-rounded" src="/assets/img/services/servises_1.jpg">
              <div class="uk-padding">Лечение натоптышей</div>
            </div>
            <div><img class="uk-border-rounded" src="/assets/img/services/servises_1.jpg">
              <div class="uk-padding">Лечение  стержневой мозоли</div>
            </div>
            <div><img class="uk-border-rounded" src="/assets/img/services/servises_1.jpg">
              <div class="uk-padding">Лечение трещин стоп</div>
            </div>
            <div><img class="uk-border-rounded" src="/assets/img/services/servises_1.jpg">
              <div class="uk-padding">Протезирование ногтей</div>
            </div>
            <div><img class="uk-border-rounded" src="/assets/img/services/servises_1.jpg">
              <div class="uk-padding">Медицинский маникюр у мужчин</div>
            </div>
            <div><img class="uk-border-rounded" src="/assets/img/services/servises_1.jpg">
              <div class="uk-padding">Коррекция вросшего ногтя у ребенка</div>
            </div>
            <div><img class="uk-border-rounded" src="/assets/img/services/servises_1.jpg">
              <div class="uk-padding">Массаж</div>
            </div>
            <div><img class="uk-border-rounded" src="/assets/img/services/servises_1.jpg">
              <div class="uk-padding">Медицинский маникюр</div>
            </div>
          </div>
        </div>
      </section>
      <section class="uk-section mz-bcg-light uk-section-small">
        <div class="uk-container">
          <h2 class="mz-h2 uk-text-center uk-padding">Наши преимущества</h2>
          <div class="uk-text-center uk-grid uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m uk-child-width-1-4@l uk-grid-small uk-grid-medium@m" uk-grid uk-height-match="target: &gt; div &gt; div &gt; span">
            <div>
              <div class="mz-accent-clr uk-margin-small-bottom"><span uk-icon="icon: advantages-comfort"></span></div>
              <div class="uk-padding-small">
                <p class="mz-text-mlead-bold">Комфортные процедуры </p>
                <p>Без боли и неприятных ощущений</p>
              </div>
            </div>
            <div>
              <div class="mz-accent-clr uk-margin-small-bottom"><span uk-icon="icon: advantages-sterility"></span></div>
              <div class="uk-padding-small">
                <p class="mz-text-mlead-bold">Абсолютная стерильность и безопасность процедур</p>
                <p>Абсолютно стерильные насадки, процедура не травмиру ющая — риск инфицирования клиента отсутствует</p>
              </div>
            </div>
            <div>
              <div class="mz-accent-clr uk-margin-small-bottom"><span uk-icon="icon: advantages-qualification"></span></div>
              <div class="uk-padding-small">
                <p class="mz-text-mlead-bold">Высочайшая квалификация специалистов</p>
                <p>Все наши мастера имеют специальное образование и виртуозно владеют методиками обработки стоп и ногтей</p>
              </div>
            </div>
            <div>
              <div class="mz-accent-clr uk-margin-small-bottom"><span uk-icon="icon: advantages-complex"></span></div>
              <div class="uk-padding-small">
                <p class="mz-text-mlead-bold">Комплексный подход в решении проблем</p>
                <p>Мы проводим весь необходимый комплекс процедур для устранения проблемы</p>
              </div>
            </div>
            <div>
              <div class="mz-accent-clr uk-margin-small-bottom"><span uk-icon="icon: advantages-cosmetics"></span></div>
              <div class="uk-padding-small">
                <p class="mz-text-mlead-bold">Профессиональная космоцевтика</p>
                <p>Мы используем только зарекомендовавшую себя косметическую продукцию</p>
              </div>
            </div>
            <div>
              <div class="mz-accent-clr uk-margin-small-bottom"><span uk-icon="icon: advantages-individual"></span></div>
              <div class="uk-padding-small">
                <p class="mz-text-mlead-bold">Индивидуальный подход </p>
                <p>Наши специалисты индивидуально подбирают методику в соответствии с пожеланиями клиента</p>
              </div>
            </div>
            <div>
              <div class="mz-accent-clr uk-margin-small-bottom"><span uk-icon="icon: advantages-location"></span></div>
              <div class="uk-padding-small">
                <p class="mz-text-mlead-bold">Удобство расположения и комфорт </p>
                <p>Кабинет расположен в самом центре города, олицетворяющем красоту и комфорт</p>
              </div>
            </div>
            <div>
              <div class="mz-accent-clr uk-margin-small-bottom"><span uk-icon="icon: advantages-universal"></span></div>
              <div class="uk-padding-small">
                <p class="mz-text-mlead-bold">Решаем любые проблемы, работаем со всеми категориями пациентов </p>
                <p>У нас комфортно всем пациентам любого пола, вероисповедания и возраста от 3+</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="uk-section">
        <div class="uk-container">
          <h2 class="mz-h2 uk-text-center uk-padding">Какие проблемы мы решаем?
            <svg class="uk-margin-medium-left" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 93.43 107.87" width="74">
              <use xlink:href="#my-icon" x="0" y="0"></use>
            </svg>
          </h2>
          <div class="uk-slider" uk-slider>
            <div class="uk-position-relative uk-flex uk-flex-wrap uk-flex-center">
              <div class="uk-slider-container uk-light uk-width-1-1 uk-width-5-6@m">
                <ul class="uk-width-1-1 uk-slider-items">
                  <li class="uk-width-1-1">
                    <div class="mz-bcg-accent uk-padding uk-panel">
                      <div class="uk-grid" uk-grid>
                        <div class="uk-margin-large-right uk-width-1-1 uk-width-auto@m uk-text-center">
                          <h3 class="mz-h3 uk-text-uppercase">Мозоли</h3><img class="uk-border-circle" src="/assets/img/problems/problems.jpg">
                        </div>
                        <div class="uk-width-1-1 uk-width-expand@m uk-text-left">
                          <p class="uk-padding uk-position-relative uk-padding-remove-vertical uk-padding-remove-right"> <span class="uk-position-top-left" uk-icon="icon: arrow-right"></span>Однако есть несколько распространённых ошибок, которые не дают таким разделам оказаться в топе Google. </p>
                          <p class="uk-padding uk-position-relative uk-padding-remove-vertical uk-padding-remove-right"> <span class="uk-position-top-left" uk-icon="icon: arrow-right"></span>Если умеете делать сайты, справитесь и со страницей с отзывами. </p>
                          <p class="uk-padding uk-position-relative uk-padding-remove-vertical uk-padding-remove-right"> <span class="uk-position-top-left" uk-icon="icon: arrow-right"></span>Однако есть несколько распространённых ошибок, которые не дают таким разделам оказаться в топе Google. </p>
                          <p class="uk-padding uk-position-relative uk-padding-remove-vertical uk-padding-remove-right"> <span class="uk-position-top-left" uk-icon="icon: arrow-right"></span>Если умеете делать сайты, справитесь и со страницей с отзывами. </p>
                          <p class="uk-padding uk-position-relative uk-padding-remove-vertical uk-padding-remove-right"> <span class="uk-position-top-left" uk-icon="icon: arrow-right"></span>Однако есть несколько распространённых ошибок, которые не дают таким разделам оказаться в топе Google. </p>
                          <p class="uk-padding uk-position-relative uk-padding-remove-vertical uk-padding-remove-right"> <span class="uk-position-top-left" uk-icon="icon: arrow-right"></span>Если умеете делать сайты, справитесь и со страницей с отзывами. </p>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="uk-width-1-1">
                    <div class="mz-bcg-accent uk-padding uk-panel">
                      <div class="uk-grid" uk-grid>
                        <div class="uk-margin-large-right uk-width-1-1 uk-width-auto@m uk-text-center">
                          <h3 class="mz-h3 uk-text-uppercase">Мозоли</h3><img class="uk-border-circle" src="/assets/img/problems/problems.jpg">
                        </div>
                        <div class="uk-width-1-1 uk-width-expand@m uk-text-left">
                          <p class="uk-padding uk-position-relative uk-padding-remove-vertical uk-padding-remove-right"> <span class="uk-position-top-left" uk-icon="icon: arrow-right"></span>Однако есть несколько распространённых ошибок, которые не дают таким разделам оказаться в топе Google. </p>
                          <p class="uk-padding uk-position-relative uk-padding-remove-vertical uk-padding-remove-right"> <span class="uk-position-top-left" uk-icon="icon: arrow-right"></span>Если умеете делать сайты, справитесь и со страницей с отзывами. </p>
                          <p class="uk-padding uk-position-relative uk-padding-remove-vertical uk-padding-remove-right"> <span class="uk-position-top-left" uk-icon="icon: arrow-right"></span>Однако есть несколько распространённых ошибок, которые не дают таким разделам оказаться в топе Google. </p>
                          <p class="uk-padding uk-position-relative uk-padding-remove-vertical uk-padding-remove-right"> <span class="uk-position-top-left" uk-icon="icon: arrow-right"></span>Если умеете делать сайты, справитесь и со страницей с отзывами. </p>
                          <p class="uk-padding uk-position-relative uk-padding-remove-vertical uk-padding-remove-right"> <span class="uk-position-top-left" uk-icon="icon: arrow-right"></span>Однако есть несколько распространённых ошибок, которые не дают таким разделам оказаться в топе Google. </p>
                          <p class="uk-padding uk-position-relative uk-padding-remove-vertical uk-padding-remove-right"> <span class="uk-position-top-left" uk-icon="icon: arrow-right"></span>Если умеете делать сайты, справитесь и со страницей с отзывами. </p>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="uk-visible@s uk-hidden@l uk-light"><a class="uk-position-center-left uk-position-medium uk-icon uk-slidenav-previous uk-slidenav" href="#" uk-slidenav-previous uk-slider-item="previous"></a><a class="uk-position-center-right uk-position-medium uk-icon uk-slidenav-next uk-slidenav" href="#" uk-slidenav-next uk-slider-item="next"></a></div>
              <div class="uk-visible@l"><a class="uk-position-center-left-out uk-position-small uk-icon uk-slidenav-previous uk-slidenav" href="#" uk-slidenav-previous uk-slider-item="previous"></a><a class="uk-position-center-right-out uk-position-small uk-icon uk-slidenav-next uk-slidenav" href="#" uk-slidenav-next uk-slider-item="next"></a></div>
            </div>
          </div>
        </div>
      </section>
      <section class="uk-section mz-bcg-light-pattern uk-section-small">
        <div class="uk-container">
          <h2 class="mz-h2 uk-text-center uk-padding" id="doctors">Наши специалисты</h2>
          <div class="uk-slider" uk-slider>
            <div class="uk-position-relative" tabindex="-1">
              <div class="uk-slider-container">
                <ul class="uk-slider-items uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m uk-grid uk-grid-match" uk-margin>
                  <li>
                    <div class="uk-card uk-card-default uk-flex uk-flex-column uk-flex-between">
                      <div>
                        <div class="uk-card-media-top"><img src="/assets/img/doctors/doctor@2x-80.jpg"></div>
                        <div class="uk-card-body">
                          <p class="uk-text-left"> <a class="mz-text-mlead-bold mz-common-clr" href="#">Яковлева Марина Степановна</a></p>
                          <hr class="mz-hr">
                          <p>Врач - Аппаратный педикюр </p>
                        </div>
                      </div>
                      <div class="uk-card-footer">Стаж работы: 19 лет</div>
                    </div>
                  </li>
                  <li>
                    <div class="uk-card uk-card-default uk-flex uk-flex-column uk-flex-between">
                      <div>
                        <div class="uk-card-media-top"><img src="/assets/img/doctors/doctor@2x-80.jpg"></div>
                        <div class="uk-card-body">
                          <p class="uk-text-left"> <a class="mz-text-mlead-bold mz-common-clr" href="#">Яковлева Марина Степановна</a></p>
                          <hr class="mz-hr">
                          <p>Врач - Аппаратный педикюр – процедура не только гигиеническая</p>
                        </div>
                      </div>
                      <div class="uk-card-footer">Стаж работы: 19 лет</div>
                    </div>
                  </li>
                  <li>
                    <div class="uk-card uk-card-default uk-flex uk-flex-column uk-flex-between">
                      <div>
                        <div class="uk-card-media-top"><img src="/assets/img/doctors/doctor@2x-80.jpg"></div>
                        <div class="uk-card-body">
                          <p class="uk-text-left"> <a class="mz-text-mlead-bold mz-common-clr" href="#">Яковлева Марина Степановна</a></p>
                          <hr class="mz-hr">
                          <p>Врач - Аппаратный педикюр – процедура не только гигиеническая</p>
                        </div>
                      </div>
                      <div class="uk-card-footer">Стаж работы: 19 лет</div>
                    </div>
                  </li>
                  <li>
                    <div class="uk-card uk-card-default uk-flex uk-flex-column uk-flex-between">
                      <div>
                        <div class="uk-card-media-top"><img src="/assets/img/doctors/doctor@2x-80.jpg"></div>
                        <div class="uk-card-body">
                          <p class="uk-text-left"> <a class="mz-text-mlead-bold mz-common-clr" href="#">Яковлева Марина Степановна</a></p>
                          <hr class="mz-hr">
                          <p>Врач - Аппаратный педикюр – процедура не только гигиеническая</p>
                        </div>
                      </div>
                      <div class="uk-card-footer">Стаж работы: 19 лет</div>
                    </div>
                  </li>
                  <li>
                    <div class="uk-card uk-card-default uk-flex uk-flex-column uk-flex-between">
                      <div>
                        <div class="uk-card-media-top"><img src="/assets/img/doctors/doctor@2x-80.jpg"></div>
                        <div class="uk-card-body">
                          <p class="uk-text-left"> <a class="mz-text-mlead-bold mz-common-clr" href="#">Яковлева Марина Степановна</a></p>
                          <hr class="mz-hr">
                          <p>Врач - Аппаратный педикюр – процедура не только гигиеническая</p>
                        </div>
                      </div>
                      <div class="uk-card-footer">Стаж работы: 19 лет</div>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="uk-visible@s uk-hidden@l"><a class="uk-position-center-left uk-position-medium uk-icon uk-slidenav-previous uk-slidenav" href="#" uk-slidenav-previous uk-slider-item="previous"></a><a class="uk-position-center-right uk-position-medium uk-icon uk-slidenav-next uk-slidenav" href="#" uk-slidenav-next uk-slider-item="next"></a></div>
              <div class="uk-visible@l"><a class="uk-position-center-left-out uk-position-small uk-icon uk-slidenav-previous uk-slidenav" href="#" uk-slidenav-previous uk-slider-item="previous"></a><a class="uk-position-center-right-out uk-position-small uk-icon uk-slidenav-next uk-slidenav" href="#" uk-slidenav-next uk-slider-item="next"></a></div>
            </div>
          </div>
        </div>
      </section>
      <section class="uk-section uk-section-small">
        <div class="uk-container">
          <h2 class="mz-h2 uk-text-center uk-padding">Отзывы о нас  </h2>
          <div class="uk-slider" uk-slider>
            <div class="uk-position-relative" tabindex="-1">
              <div class="uk-slider-container">
                <ul class="uk-slider-items uk-child-width-1-1 uk-child-width-1-2@s uk-grid uk-grid-match" uk-margin>
                  <li>
                    <div class="mz-border-rounded-medium mz-bcg-light-gray">
                      <div class="uk-padding">
                        <div class="uk-padding uk-padding-remove-top">
                          <p class="uk-margin-remove mz-text-mslead-bold mz-main-clr">Иванова Галина</p>
                          <p class="uk-margin-remove">12.04.2019</p>
                        </div>
                        <div class="uk-position-relative uk-margin-bottom"><span class="uk-position-top-left mz-main-clr" uk-icon="icon: chevron-double-left; ratio: 2"></span><span class="uk-position-bottom-right mz-main-clr" uk-icon="icon: chevron-double-right; ratio: 2"></span>
                          <p class="uk-padding uk-margin-remove">Если умеете делать сайты, справитесь и со страницей с отзывами. Однако есть несколько распространённых ошибок, которые не дают таким разделам оказаться в топе Google и Яндекс. Разберём ошибки в теории, а потом на практике. Если умеете делать сайты, справитесь и со страницей с отзывами. Однако есть несколько распространённых ошибок, которые не дают таким разделам оказаться в топе Google и Яндекс. Разберём ошибки в теории, а потом на практике.</p>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="mz-border-rounded-medium mz-bcg-light-gray">
                      <div class="uk-padding">
                        <div class="uk-padding uk-padding-remove-top">
                          <p class="uk-margin-remove mz-text-mslead-bold mz-main-clr">Иванова Галина</p>
                          <p class="uk-margin-remove">12.04.2019</p>
                        </div>
                        <div class="uk-position-relative uk-margin-bottom"><span class="uk-position-top-left mz-main-clr" uk-icon="icon: chevron-double-left; ratio: 2"></span><span class="uk-position-bottom-right mz-main-clr" uk-icon="icon: chevron-double-right; ratio: 2"></span>
                          <p class="uk-padding uk-margin-remove">Если умеете делать сайты, справитесь и со страницей с отзывами. Однако есть несколько распространённых ошибок, которые не дают таким разделам оказаться в топе Google и Яндекс. Разберём ошибки в теории, а потом на практике. Если умеете делать сайты, справитесь и со страницей с отзывами. Однако есть несколько распространённых ошибок, которые не дают таким разделам оказаться в топе Google и Яндекс. Разберём ошибки в теории, а потом на практике.</p>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="mz-border-rounded-medium mz-bcg-light-gray">
                      <div class="uk-padding">
                        <div class="uk-padding uk-padding-remove-top">
                          <p class="uk-margin-remove mz-text-mslead-bold mz-main-clr">Иванова Галина</p>
                          <p class="uk-margin-remove">12.04.2019</p>
                        </div>
                        <div class="uk-position-relative uk-margin-bottom"><span class="uk-position-top-left mz-main-clr" uk-icon="icon: chevron-double-left; ratio: 2"></span><span class="uk-position-bottom-right mz-main-clr" uk-icon="icon: chevron-double-right; ratio: 2"></span>
                          <p class="uk-padding uk-margin-remove">Если умеете делать сайты, справитесь и со страницей с отзывами. Однако есть несколько распространённых ошибок, которые не дают таким разделам оказаться в топе Google и Яндекс. Разберём ошибки в теории, а потом на практике. Если умеете делать сайты, справитесь и со страницей с отзывами. Однако есть несколько распространённых ошибок, которые не дают таким разделам оказаться в топе Google и Яндекс. Разберём ошибки в теории, а потом на практике.</p>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="uk-visible@s uk-hidden@m"><a class="uk-position-center-left uk-position-medium uk-icon uk-slidenav-previous uk-slidenav" href="#" uk-slidenav-previous uk-slider-item="previous"></a><a class="uk-position-center-right uk-position-medium uk-icon uk-slidenav-next uk-slidenav" href="#" uk-slidenav-next uk-slider-item="next"></a></div>
              <div class="uk-visible@m"><a class="uk-position-center-left-out uk-position-small uk-icon uk-slidenav-previous uk-slidenav" href="#" uk-slidenav-previous uk-slider-item="previous"></a><a class="uk-position-center-right-out uk-position-small uk-icon uk-slidenav-next uk-slidenav" href="#" uk-slidenav-next uk-slider-item="next"></a></div>
            </div>
          </div>
        </div>
      </section>
      <div class="inner"></div>
    </div>
{/block}