<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articles extends CI_Controller {

	private $data = array();

	function __construct() {
		parent::__construct();

		$this->data['title'] = "";
		$this->data['keywords'] = "";
		$this->data['description'] = "";

		$this->load->model('site_model');
		$this->load->helper('utilites_helper');
	}

	public function index()
	{
		$this->data['title'] = "Статьи";
		$this->data['keywords'] = "Статьи";
		$this->data['description'] = "Статьи";

		$total = $this->site_model->count_articles(null);
		$limit = 5;

		$this->load->library('pagination');
		$this->load->helper('utilites_helper');

		$config = get_pagination_config(base_url()."articles", $total, $limit);

		$this->pagination->initialize($config);
		$this->data['pagination'] = $this->pagination->create_links();

		$this->data['articles'] = $this->site_model->get_articles(null, $limit, (isset($_GET['page'])) ? ($limit*$_GET['page'] - $limit) : 0);
		for($i = 0; $i < count($this->data['articles']); $i++) {
			$this->data['articles'][$i]['added'] = date("d.m.Y", strtotime($this->data['articles'][$i]['added']));

			$this->data['articles'][$i]['section'] = $this->site_model->get_by_id('articles_sections', 'id', $this->data['articles'][$i]['id_articles_section']);

			$gallery = $this->site_model->get_all_by_id('articles_gallery', 'id_article', $this->data['articles'][$i]['id']);
			if(count($gallery) > 0)
				$this->data['articles'][$i]['thumb'] = get_image_link($gallery[0]['file'], true);
			else
				$this->data['articles'][$i]['thumb'] = "";
		}

		$part1 = (isset($_GET['page'])) ? ($limit*$_GET['page'] - $limit + 1) : 1;
		$part2 = (($part1 - 1 + $limit) > $total) ? $total : ($part1 - 1 + $limit);

		$this->data['articles_note'] = $part1. " - " .$part2. " из ".$total;

		$this->data['sections'] = $this->site_model->get_all('articles_sections');
		for ($i = 0; $i < count($this->data['sections']); $i++) {
			$this->data['sections'][$i]['count'] = $this->site_model->count_articles($this->data['sections'][$i]['id']);
		}

		$this->display_lib->display_page($this->data, "content/articles");
	}

	public function section($slug)
	{
		$section = $this->site_model->get_by_id('articles_sections', 'slug', $slug);
		if(empty($section)) $this->display_lib->display_404();

		$this->data['title'] = "Статьи - ".$section['name'];
		$this->data['keywords'] = "Статьи".$section['name'];
		$this->data['description'] = "Статьи".$section['name'];

		$total = $this->site_model->count_articles($section['id']);
		$limit = 5;

		$this->load->library('pagination');
		$this->load->helper('utilites_helper');

		$config = get_pagination_config(base_url()."articles/".$slug, $total, $limit);

		$this->pagination->initialize($config);
		$this->data['pagination'] = $this->pagination->create_links();

		$this->data['articles'] = $this->site_model->get_articles($section['id'], $limit, (isset($_GET['page'])) ? ($limit*$_GET['page'] - $limit) : 0);
		for($i = 0; $i < count($this->data['articles']); $i++) {
			$this->data['articles'][$i]['added'] = date("d.m.Y", strtotime($this->data['articles'][$i]['added']));

			$this->data['articles'][$i]['section'] = $this->site_model->get_by_id('articles_sections', 'id', $this->data['articles'][$i]['id_articles_section']);

			$gallery = $this->site_model->get_all_by_id('articles_gallery', 'id_article', $this->data['articles'][$i]['id']);
			if(count($gallery) > 0)
				$this->data['articles'][$i]['thumb'] = get_image_link($gallery[0]['file'], true);
			else
				$this->data['articles'][$i]['thumb'] = "";
		}

		$part1 = (isset($_GET['page'])) ? ($limit*$_GET['page'] - $limit + 1) : 1;
		$part2 = (($part1 - 1 + $limit) > $total) ? $total : ($part1 - 1 + $limit);

		$this->data['articles_note'] = $part1. " - " .$part2. " из ".$total;

		$this->data['sections'] = $this->site_model->get_all('articles_sections');
		for ($i = 0; $i < count($this->data['sections']); $i++) {
			$this->data['sections'][$i]['count'] = $this->site_model->count_articles($this->data['sections'][$i]['id']);
		}

		$this->display_lib->display_page($this->data, "content/articles");
	}

	public function item($section_slug, $slug)
	{
		$section = $this->site_model->get_by_id('articles_sections', 'slug', $section_slug);
		$this->data['item'] = $this->site_model->get_by_id('articles', 'slug', $slug);
		if(empty($this->data['item']) OR empty($section)) $this->display_lib->display_404();


		$this->data['title'] = $this->data['item']['title'];
		$this->data['keywords'] = $this->data['item']['title'];
		$this->data['description'] = $this->data['item']['title'];

		$this->data['item']['added'] = date("d.m.Y", strtotime($this->data['item']['added']));

		$gallery = $this->site_model->get_all_by_id('articles_gallery', 'id_article', $this->data['item']['id']);
		if(count($gallery) > 0) {
			$this->data['item']['thumb'] = get_image_link($gallery[0]['file'], true);
			$this->data['item']['image'] = get_image_link($gallery[0]['file'], false);
		} else {
			$this->data['item']['thumb'] = "";
			$this->data['item']['image'] = "";
		}

		$this->display_lib->display_page($this->data, "content/articles_item");
	}
}
