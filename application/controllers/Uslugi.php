<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uslugi extends CI_Controller {

	private $data = array();

	function __construct() {
		parent::__construct();

		$this->data['title'] = "";
		$this->data['keywords'] = "";
		$this->data['description'] = "";

		$this->load->model('site_model');
	}

	public function index()
	{
		redirect(base_url());
	}

	public function item() {
		$slug = $this->uri->segment($this->uri->total_segments());

		$section = $this->site_model->get_by_id('sections', 'slug', $slug);
		$page = $this->site_model->get_by_id('pages', 'id', $section['id_page']);

		if(empty($page) OR $page['publish'] != 1) {
			$this->display_lib->display_404();
		} else {
			$this->data['title'] = $page['title'];
			$this->data['keywords'] = $page['keywords'];
			$this->data['description'] = $page['description'];

			$this->data['html'] = $page['html'];

			$this->data['pages'] = array();
			if($this->uri->total_segments() > 2) {
				$sec = $this->site_model->get_by_id('sections', 'slug', $this->uri->segment($this->uri->total_segments() - 1));
				$links = $this->site_model->get_similar_sections($sec['id']);
				foreach ($links as $link) {
					if($link['id_page'] != null) {
						$pg['link'] = base_url();
						for($i = 1; $i < $this->uri->total_segments(); $i++) {
							$pg['link'] .= $this->uri->segment($i)."/";
						}
						$pg['link'] .= $link['slug'];
						$pg['slug'] = $link['slug'];
						$pg['title'] = $link['name'];
						$this->data['pages'][] = $pg;
					}
				}
			}

			$this->data['breadcrumbs'] = array();
			$this->data['breadcrumbs'][] = '<a href="'.base_url().'">Главная</a>';
			for($i = 1; $i < $this->uri->total_segments(); $i++) {
				$sec = $this->site_model->get_by_id('sections', 'slug', $this->uri->segment($i));
				$link = base_url();
				for($j = 1; $j <= $i; $j++) {
					$link .= $this->uri->segment($j)."/";
				}
				$link = rtrim($link, "/");
				$this->data['breadcrumbs'][] = ($sec['id_page'] == null) ? '<div class="breadcrumbs__item">'.$sec['name'].'</div>' : '<a class="breadcrumbs__item" href="'.$link.'">'.$sec['name'].'</a>';
			}

			$this->data['breadcrumbs'][] = '<div class="breadcrumbs__item">'.$section['name'].'</div>';

			$this->display_lib->display_page($this->data, "content/page");
		}
	}
}
