<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calculator extends CI_Controller {

	private $data = array();

	function __construct() {
		parent::__construct();

		$this->data['title'] = "";
		$this->data['keywords'] = "";
		$this->data['description'] = "";

		$this->load->model('site_model');
	}

	public function index()
	{
		$this->data['def_page'] = $this->site_model->get_by_id('def_pages', 'id', 'calculator');

		$this->data['title'] = $this->data['def_page']['title'];
		$this->data['keywords'] = $this->data['def_page']['keywords'];
		$this->data['description'] = $this->data['def_page']['description'];

		$this->display_lib->display_page($this->data, "content/def_page");
	}
}
