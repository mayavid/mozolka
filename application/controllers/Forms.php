<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forms extends CI_Controller {

	private $data = array();

	function __construct() {
		parent::__construct();

		$this->check_robot();

		$this->load->model('site_model');
	}

	//из футера
	public function subscribe()
	{
		if(isset($_POST['email'])) {

			$check = $this->site_model->get_by_id('subscribers', 'email', $this->input->post('email'));
			if(empty($check))
				$id = $this->site_model->insert_data('subscribers', array('email' => $this->input->post('email')));

			$this->data['status'] = true;
			$this->data['popup'] = array(
				'title' => 'Спасибо!',
				'text' => 'Ваш email добавлен в подписки'
			);

			echo json_encode($this->data);
		}
	}

	//с любой страницы
	public function order_call()
	{
		if(isset($_POST['fio']) && isset($_POST['phone'])) {
			$this->load->helper('email_helper');
			$this->load->library('email', get_email_config());

			$message = "<h3>Заказ обратного звонка!</h3>";
			$message .= "<p><b>Имя: </b>".$this->input->post('fio', TRUE)."</p>";
			$message .= "<p><b>Номер: </b>".$this->input->post('phone', TRUE)."</p>";
			$message .= "<p><b>Заказ сформирован: </b>".date('d.m.Y H:i', time())."</p>";
			$message .= "<p><center><small>Письмо сформировано автоматически, отвечать на него не надо.</small></center></p>";

			$this->email->clear();
			$this->email->to(get_email_config()['smtp_user']);
			$this->email->from(get_email_config()['smtp_user'], "«БиПи Аутсорсинг бизнес-процессов»");
			$this->email->subject("Заказ обратного звонка");
			$this->email->message($message);
			$this->email->send();

			$this->data['status'] = true;
			$this->data['popup'] = array(
				'title' => 'Спасибо!',
				'text' => 'С Вами свяжутся по указанному номеру в ближайшее время.'
			);

			echo json_encode($this->data);
		}
	}

	//с любой страницы
	public function order_service()
	{
		if(isset($_POST['fio']) && isset($_POST['phone']) && isset($_POST['email']) && isset($_POST['message'])) {
			$this->load->helper('email_helper');
			$this->load->library('email', get_email_config());

			$message = "<h3>Заказ услуги!</h3>";
			$message .= "<p><b>Имя: </b>".$this->input->post('fio', TRUE)."</p>";
			$message .= "<p><b>Номер: </b>".$this->input->post('phone', TRUE)."</p>";
			$message .= "<p><b>Комментарий: </b>".$this->input->post('message', TRUE)."</p>";
			$message .= "<p><b>Заказ сформирован: </b>".date('d.m.Y H:i', time())."</p>";
			$message .= "<p><center><small>Письмо сформировано автоматически, отвечать на него не надо.</small></center></p>";

			$this->email->clear();

			if (isset($_FILES['files']) AND count($_FILES['files']['name']) > 0) {
				for($i = 0; $i < count($_FILES['files']['name']); $i++) {
					if ($_FILES['files']['error'][$i] == 0) {
						$this->email->attach($_FILES['files']['tmp_name'][$i], 'attachment', $_FILES['files']['name'][$i]);
					}
				}
			}

			$this->email->to(get_email_config()['smtp_user']);
			$this->email->from(get_email_config()['smtp_user'], "«БиПи Аутсорсинг бизнес-процессов»");
			$this->email->subject("Заказ услуги");
			$this->email->message($message);
			$this->email->send();

			$this->data['status'] = true;
			$this->data['popup'] = array(
				'title' => 'Спасибо!',
				'text' => 'С Вами свяжутся по указанному номеру или email в ближайшее время.'
			);

			echo json_encode($this->data);
		}
	}

	//со страницы контакты
	public function question()
	{
		if(isset($_POST['fio']) && isset($_POST['phone']) && isset($_POST['message'])) {
			$this->load->helper('email_helper');
			$this->load->library('email', get_email_config());

			$message = "<h3>Вопрос специалисту!</h3>";
			$message .= "<p><b>Имя: </b>".$this->input->post('fio', TRUE)."</p>";
			$message .= "<p><b>Номер: </b>".$this->input->post('phone', TRUE)."</p>";
			$message .= "<p><b>Сообщение: </b>".$this->input->post('message', TRUE)."</p>";
			$message .= "<p><b>Вопрос сформирован: </b>".date('d.m.Y H:i', time())."</p>";
			$message .= "<p><center><small>Письмо сформировано автоматически, отвечать на него не надо.</small></center></p>";

			$this->email->clear();
			$this->email->to(get_email_config()['smtp_user']);
			$this->email->from(get_email_config()['smtp_user'], "«БиПи Аутсорсинг бизнес-процессов»");
			$this->email->subject("Вопрос специалисту");
			$this->email->message($message);
			$this->email->send();

			$this->data['status'] = true;
			$this->data['popup'] = array(
				'title' => 'Спасибо!',
				'text' => 'С Вами свяжутся по указанному номеру в ближайшее время.'
			);

			echo json_encode($this->data);
		}
	}

	private function check_robot() {

		if(!isset($_POST['g-recaptcha-response'])) {
			$this->data['status'] = false;
			$this->data['popup'] = array(
				'title' => 'Ошибка!',
				'text' => 'В форме не передавалась рекапча.'
			);

			exit(json_encode($this->data));
		}

		$this->load->helper('utilites_helper');
		$verify = getReCaptchaV3($_POST['g-recaptcha-response']);

		if ($verify->success != true OR $verify->score <= 0.5){
			$this->data['status'] = false;
			$this->data['popup'] = array(
				'title' => 'Ошибка!',
				'text' => 'Система определила, что вы робот.'
			);

			exit(json_encode($this->data));
		}
	}
}
