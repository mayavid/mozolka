<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	private $data = array();

	function __construct() {
		parent::__construct();

		$this->data['title'] = "";
		$this->data['keywords'] = "";
		$this->data['description'] = "";

		$this->load->model('site_model');
		$this->load->helper('utilites_helper');
	}

	public function index()
	{
		$this->data['title'] = "Новости";
		$this->data['keywords'] = "Новости";
		$this->data['description'] = "Новости";

		$total = $this->site_model->count_news();
		$limit = 5;

		$this->load->library('pagination');
		$this->load->helper('utilites_helper');

		$config = get_pagination_config(base_url()."news", $total, $limit);

		$this->pagination->initialize($config);
		$this->data['pagination'] = $this->pagination->create_links();

		$this->data['news'] = $this->site_model->get_news($limit, (isset($_GET['page'])) ? ($limit*$_GET['page'] - $limit) : 0);
		for($i = 0; $i < count($this->data['news']); $i++) {
			$this->data['news'][$i]['added'] = date("d.m.Y", strtotime($this->data['news'][$i]['added']));

			$gallery = $this->site_model->get_all_by_id('news_gallery', 'id_new', $this->data['news'][$i]['id']);
			if(count($gallery) > 0)
				$this->data['news'][$i]['thumb'] = get_image_link($gallery[0]['file'], true);
			else
				$this->data['news'][$i]['thumb'] = "";
		}

		$part1 = (isset($_GET['page'])) ? ($limit*$_GET['page'] - $limit + 1) : 1;
		$part2 = (($part1 - 1 + $limit) > $total) ? $total : ($part1 - 1 + $limit);

		$this->data['news_note'] = $part1. " - " .$part2. " из ".$total;

		$this->display_lib->display_page($this->data, "content/news");
	}

	public function item($slug)
	{
		$this->data['item'] = $this->site_model->get_by_id('news', 'slug', $slug);
		if(empty($this->data['item'])) {
			$this->display_lib->display_404();
			return;
		}

		$this->data['title'] = $this->data['item']['title'];
		$this->data['keywords'] = $this->data['item']['title'];
		$this->data['description'] = $this->data['item']['title'];

		$this->data['item']['added'] = date("d.m.Y", strtotime($this->data['item']['added']));

		$gallery = $this->site_model->get_all_by_id('news_gallery', 'id_new', $this->data['item']['id']);
		if(count($gallery) > 0) {
			$this->data['item']['thumb'] = get_image_link($gallery[0]['file'], true);
			$this->data['item']['image'] = get_image_link($gallery[0]['file'], false);
		} else {
			$this->data['item']['thumb'] = "";
			$this->data['item']['image'] = "";
		}

		$this->display_lib->display_page($this->data, "content/news_item");
	}
}
