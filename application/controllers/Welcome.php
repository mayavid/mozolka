<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	private $data = array();

	function __construct() {
		parent::__construct();

		$this->data['title'] = "";
		$this->data['keywords'] = "";
		$this->data['description'] = "";

		$this->load->model('site_model');
	}

	public function index()
	{
		$this->data['title'] = "Главная";
		$this->data['keywords'] = "Главная";
		$this->data['description'] = "Главная";

		$this->display_lib->display_page($this->data, "content/index");
	}

	public function change_city() {
		$data = array();
		if(isset($_POST['city']) AND in_array($_POST['city'], array('spb', 'msk'))) {
			$this->session->set_userdata('city', $_POST['city']);
			$data['status'] = true;
		} else {
			$data['status'] = false;
		}

		echo json_encode($data);
	}
}
