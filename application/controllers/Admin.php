<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/*:::Коды ошибок:::*/
    const NO_ERRORS                             = 0;        //Нет ошибок
    const REQUEST_ERROR                         = 1;        //Ошибка запроса
    const LOGIN_ERROR                           = 2;        //Неверные логин и пароль
    const AUTH_NEEDED                           = 3;        //Требуется авторизация
    const ACCESS_DENIED                         = 4;        //Нет доступа к данному разделу
    const RECORD_NOT_FOUND                      = 5;        //Запись в БД не найдена
    const CANT_DELETE                           = 6;        //Запись невозможно удалить из БД
    const IMAGE_UPLOAD_ERROR                    = 7;        //Изображение невозможно загрузить
    const IMAGE_RESIZE_ERROR                    = 8;        //Невозможно изменить размер изображения
    const IMAGE_ROTATE_ERROR                    = 9;        //Невозможно повернуть изображение
    const SLUG_ALREADY_USED                     = 10;       //Данный slug уже используется
    const SLUG_CONTAINS_DISALLOWED_CHARACTERS   = 11;       //slug содержит неразрешённые символы

	/*:::Директории:::*/
	const DIR_IMAGES			= "./assets/uploads/images/";
	const DIR_THUMBS			= "./assets/uploads/thumbs/";

    private $admin;

    private $json = array();

    function __construct() {
        parent::__construct();

        $this->json['status'] = self::NO_ERRORS;

        $this->load->helper('utilites_helper');
        $this->load->model('admin_model');
    }

    public function index() {
    	$this->paranoya();

		if ($this->input->post('action') == NULL) {
			$this->load->view('admin');
		} else {
			$method = array($this, $this->input->post('action'));

			if(is_callable($method)) {
				call_user_func($method);
			} else {
				$this->json['status'] = self::REQUEST_ERROR;
				exit(json_encode($this->json));
			}
		}
    }

    /*:::Авторизация:::*/

    private function login()
    {
        $this->admin = $this->admin_model->get_admin_by_credentials($this->input->post('login'), $this->input->post('password'));

        if(empty($this->admin)) {
            $this->json['status'] = self::LOGIN_ERROR;
        } else {
            $this->session->set_userdata('admin_token', $this->admin['token']);
        }

        echo json_encode($this->json);
    }

	private function logout()
    {
        $this->session->unset_userdata('admin_token');

        echo json_encode($this->json);
    }

    private function paranoya()
	{
		$check = true;
		if (!$this->session->has_userdata('admin_token')) {
			$check = false;
		}

		if($check) {
			$this->admin = $this->admin_model->get_admin_by_token($this->session->userdata('admin_token'));
			if(empty($this->admin)) {
				$check = false;
			}
		}

		if(!$check && $this->uri->segment(2) == 'c9760b84e2' && $this->uri->segment(3) == 'in')
			$check = true;

		if(!$check && $this->input->post('action') != NULL && ($this->input->post('action') == 'login' || $this->input->post('action') == 'check_auth'))
			$check = true;

		if(!$check)
			redirect(base_url()."error_404");
	}

	private function check_auth($from_func = false)
    {
        if (!$this->session->has_userdata('admin_token')) {
            $this->json['status'] = self::AUTH_NEEDED;
            exit(json_encode($this->json));
        }

        $this->admin = $this->admin_model->get_admin_by_token($this->session->userdata('admin_token'));

        if(empty($this->admin)) {
            $this->session->unset_userdata('admin_token');
            $this->json['status'] = self::AUTH_NEEDED;
            exit(json_encode($this->json));
        }

        if(!$from_func) {
			echo json_encode($this->json);
		}
    }

    private function get_current_admin()
	{
		$this->check_auth(true);

		$this->json['data'] = $this->admin;

		echo json_encode($this->json);
	}

	/*:::Главная:::*/

	private function main()
	{
		$this->check_auth(true);

		$this->json['data'][0]['key'] = 'Количество страниц в базе';
		$this->json['data'][0]['value'] = $this->admin_model->count_all('pages');

		$this->json['data'][1]['key'] = 'Количество новостей в базе';
		$this->json['data'][1]['value'] = $this->admin_model->count_all('news');

		$this->json['data'][1]['key'] = 'Количество статей в базе';
		$this->json['data'][1]['value'] = $this->admin_model->count_all('articles');

		$this->load->helper('directory');
		$this->load->helper('file');
		$this->load->helper('number');

		$map = directory_map(self::DIR_IMAGES, 0);

		$counter = 0;
		$size = 0;
		foreach ($map as $dir) {
			if(is_array($dir)) {
				foreach ($dir as $sdir) {
					if(is_array($sdir)) {
						foreach ($sdir as $file) {
							$d1 = mb_substr($file, 0, 2);
							$d2 = mb_substr($file, 2, 2);
							$image = self::DIR_IMAGES.$d1."/".$d2."/".$file;
							$thumb = self::DIR_THUMBS.$d1."/".$d2."/".$file;

							$s = get_file_info($image, array('size'));
							$size += $s['size'];
							$s = get_file_info($thumb, array('size'));
							$size += $s['size'];

							$counter++;
						}
					}
				}
			}
		}

		$this->json['data'][2]['key'] = 'Количество изображений (вместе с превьюшками)';
		$this->json['data'][2]['value'] = $counter;

		$this->json['data'][3]['key'] = 'Место занимаемое изображениями';
		$this->json['data'][3]['value'] = byte_format($size);

		echo json_encode($this->json);
	}

	/*:::Вспомогательные функции:::*/

	private function check_slug($slug)
	{
		if(!preg_match('/^['.$this->config->item('permitted_uri_chars').']+$/i'.(UTF8_ENABLED ? 'u' : ''), $slug)) {
			$this->json['status'] = self::SLUG_CONTAINS_DISALLOWED_CHARACTERS;
			exit(json_encode($this->json));
		}
	}

	/*:::Администраторы:::*/

	private function get_admins()
    {
        $this->check_auth(true);

        if($this->admin['super'] != 1) {
            $this->json['status'] = self::ACCESS_DENIED;
            exit(json_encode($this->json));
        }

        $this->json['data'] = $this->admin_model->get_all('admin_users');

        echo json_encode($this->json);
    }

	private function get_admin()
    {
        $this->check_auth(true);

        if($this->admin['super'] != 1) {
            $this->json['status'] = self::ACCESS_DENIED;
            exit(json_encode($this->json));
        }

        $item = $this->admin_model->get_by_id('admin_users', 'id', $this->input->post('id'));
        if(empty($item)) {
            $this->json['status'] = self::RECORD_NOT_FOUND;
            exit(json_encode($this->json));
        }

        $this->json['data'] = $item;

        echo json_encode($this->json);
    }

	private function refresh_token()
    {
        $this->check_auth(true);

        if($this->admin['super'] != 1) {
            $this->json['status'] = self::ACCESS_DENIED;
            exit(json_encode($this->json));
        }

        $token = generate_uuid_v4();
        $this->admin_model->update_by_id('admin_users', 'id', $this->input->post('id'), array('token' => $token));
        if($this->admin['id'] == $this->input->post('id'))
            $this->session->set_userdata('token', $token);

        echo json_encode($this->json);
    }

	private function add_admin()
    {
        $this->check_auth(true);

        if($this->admin['super'] != 1) {
            $this->json['status'] = self::ACCESS_DENIED;
            exit(json_encode($this->json));
        }

        $insdata = array(
            'token' => generate_uuid_v4(),
            'login' => $this->input->post('login'),
            'password' => $this->input->post('password'),
            'email' => $this->input->post('email'),
            'fio' => $this->input->post('fio'),
            'super' => 0
        );

        $id = $this->admin_model->insert_data('admin_users', $insdata);

        echo json_encode($this->json);
    }

	private function edit_admin()
    {
        $this->check_auth(true);

        if($this->admin['super'] != 1) {
            $this->json['status'] = self::ACCESS_DENIED;
            exit(json_encode($this->json));
        }

        $item = $this->admin_model->get_by_id('admin_users', 'id', $this->input->post('id'));

        if(empty($item)) {
            $this->json['status'] = self::RECORD_NOT_FOUND;
            exit(json_encode($this->json));
        }

        $token = generate_uuid_v4();
        $upddata = array(
            'token' => $token,
            'login' => $this->input->post('login'),
            'password' => $this->input->post('password'),
			'email' => $this->input->post('email'),
            'fio' => $this->input->post('fio')
        );

        $this->admin_model->update_by_id('admin_users', 'id', $item['id'], $upddata);

        if($this->admin['id'] == $item['id'])
            $this->session->set_userdata('token', $token);

        echo json_encode($this->json);
    }

	private function delete_admin()
    {
        $this->check_auth(true);

        if($this->admin['super'] != 1) {
            $this->json['status'] = self::ACCESS_DENIED;
            exit(json_encode($this->json));
        }

        $item = $this->admin_model->get_by_id('admin_users', 'id', $this->input->post('id'));

        if(empty($item))
            $this->json['status'] = self::RECORD_NOT_FOUND;
        elseif ($item['super'])
            $this->json['status'] = self::CANT_DELETE;
        else
            $this->admin_model->delete_by_id('admin_users', 'id', $item['id']);

        echo json_encode($this->json);
    }

    /*:::Изображения:::*/

	private function upload_image()
	{
		$this->check_auth(true);

		$u_config['upload_path'] = self::DIR_IMAGES;
		$u_config['allowed_types'] = 'jpg|jpeg|png|gif';
		$u_config['max_size'] = 2048;
		$u_config['max_filename'] = '255';
		$u_config['encrypt_name'] = TRUE;

		$this->load->library('upload', $u_config);

		if (!$this->upload->do_upload('file')) {
			$this->json['status'] = self::IMAGE_UPLOAD_ERROR;
			$this->json['data'] = $this->upload->display_errors();
		} else {
			$d1 = mb_substr($this->upload->data('file_name'), 0, 2);
			$d2 = mb_substr($this->upload->data('file_name'), 2, 2);

			$dir1 = self::DIR_IMAGES.$d1."/";
			$dir2 = self::DIR_IMAGES.$d1."/".$d2."/";

			if (!file_exists($dir1))
				mkdir($dir1, 0755);
				if (!file_exists($dir2))
					mkdir($dir2, 0755);

			$D = getimagesize($this->upload->data('full_path'));
			if($D[0]>1920)
				$i_config['width'] = 1920;
			if($D[1]>1080)
				$i_config['height'] = 1080;
			$i_config['image_library'] = 'gd2';
			$i_config['source_image'] = $this->upload->data('full_path');
			$i_config['maintain_ratio'] = TRUE;
			$i_config['new_image'] = $dir2.$this->upload->data('file_name');

			$this->load->library('image_lib', $i_config);

			if (!$this->image_lib->resize()) {
				$this->json['status'] = self::IMAGE_RESIZE_ERROR;
				$this->json['data'] = $this->image_lib->display_errors();
			} else {
				$dir3 = self::DIR_THUMBS.$d1."/";
				$dir4 = self::DIR_THUMBS.$d1."/".$d2."/";

				if (!file_exists($dir3))
					mkdir($dir3, 0755);
				if (!file_exists($dir4))
					mkdir($dir4, 0755);

				$i_config['image_library'] = 'gd2';
				$i_config['source_image'] = $this->upload->data('full_path');
				$i_config['maintain_ratio'] = TRUE;
				$i_config['new_image'] = $dir4.$this->upload->data('file_name');
				$i_config['width'] = 200;
				$i_config['height'] = 200;

				$this->image_lib->initialize($i_config);

				if (!$this->image_lib->resize()) {
					$this->json['status'] = self::IMAGE_RESIZE_ERROR;
					$this->json['data'] = $this->image_lib->display_errors();
				} else {
					$this->json['data'] = $this->get_image_links($this->upload->data('file_name'));
				}
			}

			unlink($this->upload->data('full_path'));
		}

		echo json_encode($this->json);
	}

	private function rotate_image()
	{
		$this->check_auth(true);

		//PHP вращает против часовой стрелки, поэтому поворот на 90 градусов вправо должен быть указан как 270.
		$angle = 360 - $this->input->post('angle');

		$d1 = mb_substr($this->input->post('file'), 0, 2);
		$d2 = mb_substr($this->input->post('file'), 2, 2);

		$image_path = self::DIR_IMAGES.$d1."/".$d2."/";
		$thumb_path = self::DIR_THUMBS.$d1."/".$d2."/";

		$i_config['image_library'] = 'gd2';
		$i_config['source_image'] = $image_path.$this->input->post('file');
		$i_config['rotation_angle'] = $angle;

		$this->load->library('image_lib', $i_config);

		if (!$this->image_lib->rotate()) {
			$this->json['status'] = self::IMAGE_ROTATE_ERROR;
			$this->json['data'] = $this->image_lib->display_errors();
		} else {
			$i_config['source_image'] = $thumb_path.$this->input->post('file');

			$this->image_lib->clear();
			$this->image_lib->initialize($i_config);

			if (!$this->image_lib->rotate()) {
				$this->json['status'] = self::IMAGE_ROTATE_ERROR;
				$this->json['data'] = $this->image_lib->display_errors();
			}
		}
	}

	private function delete_image()
	{
		$this->check_auth();

		$d1 = mb_substr($this->input->post('file'), 0, 2);
		$d2 = mb_substr($this->input->post('file'), 2, 2);

		$image = self::DIR_IMAGES.$d1."/".$d2."/".$this->input->post('file');
		$thumb = self::DIR_THUMBS.$d1."/".$d2."/".$this->input->post('file');

		if (file_exists($image))
			unlink($image);

		if (file_exists($thumb))
			unlink($thumb);

		echo json_encode($this->json);
	}

	private function inner_rotate_image($file, $angle)
	{
		if(!$this->load->is_loaded('image_lib'))
			$this->load->library('image_lib');

		//PHP вращает против часовой стрелки, поэтому поворот на 90 градусов вправо должен быть указан как 270.
		$angle = 360 - $angle;

		$d1 = mb_substr($file, 0, 2);
		$d2 = mb_substr($file, 2, 2);

		$image_path = self::DIR_IMAGES.$d1."/".$d2."/";
		$thumb_path = self::DIR_THUMBS.$d1."/".$d2."/";

		$i_config['image_library'] = 'gd2';
		$i_config['source_image'] = $image_path.$file;
		$i_config['rotation_angle'] = $angle;

		$this->image_lib->clear();
		$this->image_lib->initialize($i_config);

		if ($this->image_lib->rotate()) {
			$i_config['source_image'] = $thumb_path.$file;

			$this->image_lib->clear();
			$this->image_lib->initialize($i_config);

			$this->image_lib->rotate();
		}
	}

	private function inner_delete_image($file)
	{
		$d1 = mb_substr($file, 0, 2);
		$d2 = mb_substr($file, 2, 2);

		$image = self::DIR_IMAGES.$d1."/".$d2."/".$file;
		$thumb = self::DIR_THUMBS.$d1."/".$d2."/".$file;

		if (file_exists($image))
			unlink($image);

		if (file_exists($thumb))
			unlink($thumb);
	}

	private function get_image_links($file)
	{
		$links = array(
			'id' => '',
			'url' => '',
			'thumb' => ''
		);

		if(!empty($file)) {
			$d1 = mb_substr($file, 0, 2);
			$d2 = mb_substr($file, 2, 2);

			$md5_image = md5_file(self::DIR_IMAGES .$d1."/".$d2."/". $file);
			$md5_thumb = md5_file(self::DIR_THUMBS .$d1."/".$d2."/". $file);

			$links['id'] = $file;
			$links['url'] = base_url() . mb_substr(self::DIR_IMAGES, 2) .$d1."/".$d2."/". $file."?".$md5_image;
			$links['thumb'] = base_url() . mb_substr(self::DIR_THUMBS, 2) .$d1."/".$d2."/". $file."?".$md5_thumb;
		}

		return $links;
	}

	private function handler_image($file) {
		if(isset($file['removed']) AND $file['removed'] == true) {
			$this->inner_delete_image($file['id']);
			return "";
		}

		if(isset($file['rotate']))
			$this->inner_rotate_image($file['id'], $file['rotate']);

		return $file['id'];
	}

    private function clear_cache()
    {
        $this->check_auth(true);

        $used_images = array();

        $items = $this->admin_model->get_all('pages_gallery');
        foreach ($items as $item)
            $used_images[] = $item['file'];

		$items = $this->admin_model->get_all('news_gallery');
		foreach ($items as $item)
			$used_images[] = $item['file'];

		$items = $this->admin_model->get_all('articles_gallery');
		foreach ($items as $item)
			$used_images[] = $item['file'];

        $this->load->helper('directory');
        $this->load->helper('file');
        $this->load->helper('number');

        $map = directory_map(self::DIR_IMAGES, 0);

        $counter = 0;
        $size = 0;
        foreach ($map as $dir) {
            if(is_array($dir)) {
                foreach ($dir as $sdir) {
                    if(is_array($sdir)) {
                        foreach ($sdir as $file) {
                            if(!in_array($file, $used_images)) {
                                $d1 = mb_substr($file, 0, 2);
                                $d2 = mb_substr($file, 2, 2);
                                $image = self::DIR_IMAGES.$d1."/".$d2."/".$file;
                                $thumb = self::DIR_THUMBS.$d1."/".$d2."/".$file;

                                $s = get_file_info($image, array('size'));
                                $size += $s['size'];
                                $s = get_file_info($thumb, array('size'));
                                $size += $s['size'];

                                $this->inner_delete_image($file);
                                $counter++;
                            }
                        }
                    }
                }
            }
        }

        $this->json['count'] = $counter;
        $this->json['size'] = byte_format($size);

        echo json_encode($this->json);
    }

    /*:::Секции:::*/

    private function get_sections()
    {
        $this->check_auth(true);

        $sections = $this->admin_model->get_all_sort('sections', 'rating', 'DESC');

		for ($i = 0; $i < count($sections); $i++) {

			$sections[$i]['publish'] = ($sections[$i]['publish'] == 1) ? true : false;

			if($sections[$i]['id_page'] != NULL) {
				$sections[$i]['page'] = $this->admin_model->get_short_page('pages', 'id', $sections[$i]['id_page']);
				$sections[$i]['page']['publish'] = ($sections[$i]['page']['publish'] == 1) ? true : false;
			} else {
				$sections[$i]['page'] = array();
			}
		}

        $tree = map_tree($sections);

		$map = array();
		$this->parse_tree($tree, $map, 0);

		$this->json['data'] = $map;

        echo json_encode($this->json);
    }

    private function parse_tree($tree, &$map, $level)
	{
		foreach ($tree as $item) {
			$item['level'] = $level;
			$map[] = $item;
			if(isset($item['childs'])) {
				$this->parse_tree($item['childs'],$map, $level+1);
			}
		}
	}

    private function get_section()
    {
        $this->check_auth(true);

        $item = $this->admin_model->get_by_id('sections', 'id', $this->input->post('id'));
        if(empty($item)) {
            $this->json['status'] = self::RECORD_NOT_FOUND;
            exit(json_encode($this->json));
        }

		$item['publish'] = ($item['publish'] == 1) ? true : false;

		if($item['id_page'] != NULL) {
			$item['page'] = $this->admin_model->get_short_page('pages', 'id', $item['id_page']);
			$item['page']['publish'] = ($item['page']['publish'] == 1) ? true : false;
		} else {
			$item['page'] = array();
		}

        $this->json['data'] = $item;

        echo json_encode($this->json);
    }

    private function add_section()
    {
        $this->check_auth(true);

        if(!preg_match('/^['.$this->config->item('permitted_uri_chars').']+$/i'.(UTF8_ENABLED ? 'u' : ''), $this->input->post('slug'))) {
            $this->json['status'] = self::SLUG_CONTAINS_DISALLOWED_CHARACTERS;
            exit(json_encode($this->json));
        }

        $slug = $this->admin_model->get_by_id('sections', 'slug', $this->input->post('slug'));
        if(!empty($slug)) {
            $this->json['status'] = self::SLUG_ALREADY_USED;
            exit(json_encode($this->json));
        }

        $insdata = array(
            'parent' => ($this->input->post('parent') != null && !empty($this->input->post('parent'))) ? $this->input->post('parent') : 0,
            'name' => $this->input->post('name'),
            'slug' => $this->input->post('slug'),
			'publish' => ($this->input->post('publish') == 'true') ? 1 : 0,
			'id_page' => (empty($this->input->post('id_page')) OR $this->input->post('id_page') == 'null') ? null : $this->input->post('id_page'),
			'rating' => ($this->input->post('rating') != NULL AND !empty($this->input->post('rating'))) ? $this->input->post('rating') : 0,

        );

        $id = $this->admin_model->insert_data('sections', $insdata);

        echo json_encode($this->json);
    }

    private function edit_section()
    {
        $this->check_auth(true);

        $item = $this->admin_model->get_by_id('sections', 'id', $this->input->post('id'));

        if(empty($item)) {
            $this->json['status'] = self::RECORD_NOT_FOUND;
            exit(json_encode($this->json));
        }

        if(!preg_match('/^['.$this->config->item('permitted_uri_chars').']+$/i'.(UTF8_ENABLED ? 'u' : ''), $this->input->post('slug'))) {
            $this->json['status'] = self::SLUG_CONTAINS_DISALLOWED_CHARACTERS;
            exit(json_encode($this->json));
        }

        $slug = $this->admin_model->get_by_id('sections', 'slug', $this->input->post('slug'));
        if(!empty($slug) AND $this->input->post('id') != $slug['id']) {
            $this->json['status'] = self::SLUG_ALREADY_USED;
            exit(json_encode($this->json));
        }

        $upddata = array(
			'parent' => ($this->input->post('parent') != null && !empty($this->input->post('parent'))) ? $this->input->post('parent') : 0,
			'name' => $this->input->post('name'),
			'slug' => $this->input->post('slug'),
			'publish' => ($this->input->post('publish') == 'true') ? 1 : 0,
			'id_page' => (empty($this->input->post('id_page')) OR $this->input->post('id_page') == 'null') ? null : $this->input->post('id_page'),
			'rating' => ($this->input->post('rating') != NULL AND !empty($this->input->post('rating'))) ? $this->input->post('rating') : 0
        );

        $this->admin_model->update_by_id('sections', 'id', $item['id'], $upddata);

        echo json_encode($this->json);
    }

    private function delete_section()
    {
        $this->check_auth(true);

        $item = $this->admin_model->get_by_id('sections', 'id', $this->input->post('id'));

        if(empty($item)) {
            $this->json['status'] = self::RECORD_NOT_FOUND;
        } else {
            $sections = prepare_tree($this->crud_model->get_all_sort('sections', 'id', 'ASC'));
            $ids = cats_id($sections, $item['id']);
            $ids = !$ids ? $item['id'] : rtrim($ids, ",");
            $ids = explode(',', $ids);
            $this->admin_model->delete_by_array('sections', 'id', $ids);
            $this->admin_model->delete_by_id('sections', 'id', $item['id']);
        }

        echo json_encode($this->json);
    }

    /*:::Страницы:::*/

    private function get_pages()
    {
        $this->check_auth(true);

        $this->json['data'] = $this->admin_model->get_all('pages');
        for ($i = 0; $i < count($this->json['data']); $i++) {
			$this->json['data'][$i]['publish'] = ($this->json['data'][$i]['publish'] == 1) ? true : false;

            $gallery = $this->admin_model->get_all_by_id('pages_gallery', 'id_page', $this->json['data'][$i]['id']);
            $this->json['data'][$i]['gallery'] = array();
            foreach ($gallery as $image)
                $this->json['data'][$i]['gallery'][] = $this->get_image_links($image['file']);

			$pages_routing = $this->admin_model->get_all_by_id('pages_routing', 'id_parent_page', $this->json['data'][$i]['id']);
			$this->json['data'][$i]['routing'] = array();
			foreach ($pages_routing as $page) {
				$item = $this->admin_model->get_short_page('pages', 'id', $page['id_page']);
				$item['publish'] = ($item['publish'] == 1) ? true : false;
				$this->json['data'][$i]['routing'][] = $item;
			}
        }

        echo json_encode($this->json);
    }

	private function get_short_pages()
	{
		$this->check_auth(true);

		$this->json['data'] = $this->admin_model->get_short_pages();
		for ($i = 0; $i < count($this->json['data']); $i++) {
			$this->json['data'][$i]['publish'] = ($this->json['data'][$i]['publish'] == 1) ? true : false;
		}

		echo json_encode($this->json);
	}

    private function get_page()
    {
        $this->check_auth(true);

        $item = $this->admin_model->get_by_id('pages', 'id', $this->input->post('id'));
        if(empty($item)) {
            $this->json['status'] = self::RECORD_NOT_FOUND;
            exit(json_encode($this->json));
        }

        $this->json['data'] = $item;

		$this->json['data']['publish'] = ($this->json['data']['publish'] == 1) ? true : false;

        $gallery = $this->admin_model->get_all_by_id('pages_gallery', 'id_page', $this->json['data']['id']);
        $this->json['data']['gallery'] = array();
        foreach ($gallery as $image)
            $this->json['data']['gallery'][] = $this->get_image_links($image['file']);

		$pages_routing = $this->admin_model->get_all_by_id('pages_routing', 'id_parent_page', $this->json['data']['id']);
		$this->json['data']['routing'] = array();
		foreach ($pages_routing as $page) {
			$item = $this->admin_model->get_short_page($page['id_page']);
			$item['publish'] = ($item['publish'] == 1) ? true : false;
			$this->json['data']['routing'][] = $item;
		}

        echo json_encode($this->json);
    }

	private function get_short_page()
	{
		$this->check_auth(true);

		$item = $this->admin_model->get_short_page($this->input->post('id'));
		if(empty($item)) {
			$this->json['status'] = self::RECORD_NOT_FOUND;
			exit(json_encode($this->json));
		}

		$this->json['data'] = $item;

		$this->json['data']['publish'] = ($this->json['data']['publish'] == 1) ? true : false;

		echo json_encode($this->json);
	}

    private function add_page()
    {
        $this->check_auth(true);

        $insdata = array(
            'title' => $this->input->post('title'),
            'keywords' => $this->input->post('keywords'),
            'description' => $this->input->post('description'),
            'html' => $this->input->post('html'),
            'publish' => ($this->input->post('publish') == 'true') ? 1 : 0
        );

        $id = $this->admin_model->insert_data('pages', $insdata);

        if($this->input->post('gallery') != NULL AND is_array($this->input->post('gallery'))) {
            foreach ($this->input->post('gallery') as $file) {
                $file_id = $this->handler_image(json_decode($file, true));
                if(!empty($file_id))
                    $this->admin_model->insert_data('pages_gallery', array('id_page' => $id, 'file' => $file_id));
            }
        }

		if($this->input->post('routing') != NULL AND is_array($this->input->post('routing'))) {
			foreach ($this->input->post('routing') as $route) {
				$this->admin_model->insert_data('pages_routing', array('id_parent_page' => $id, 'id_page' => $route));
			}
		}

        echo json_encode($this->json);
    }

    private function edit_page()
    {
        $this->check_auth(true);

        $item = $this->admin_model->get_by_id('pages', 'id', $this->input->post('id'));

        if(empty($item)) {
            $this->json['status'] = self::RECORD_NOT_FOUND;
            exit(json_encode($this->json));
        }

        $upddata = array(
            'title' => $this->input->post('title'),
            'keywords' => $this->input->post('keywords'),
            'description' => $this->input->post('description'),
            'html' => $this->input->post('html'),
            'publish' => ($this->input->post('publish') == 'true') ? 1 : 0
        );

        $this->admin_model->update_by_id('pages', 'id', $item['id'], $upddata);

        $this->admin_model->delete_by_id('pages_gallery', 'id_page', $item['id']);
        if($this->input->post('gallery') != NULL AND is_array($this->input->post('gallery'))) {
            foreach ($this->input->post('gallery') as $file) {
                $file_id = $this->handler_image(json_decode($file, true));
                if(!empty($file_id))
                    $this->admin_model->insert_data('pages_gallery', array('id_page' => $item['id'], 'file' => $file_id));
            }
        }

		$this->admin_model->delete_by_id('pages_routing', 'id_parent_page', $item['id']);
		if($this->input->post('routing') != NULL AND is_array($this->input->post('routing'))) {
			foreach ($this->input->post('routing') as $route) {
				$this->admin_model->insert_data('pages_routing', array('id_parent_page' => $item['id'], 'id_page' => $route));
			}
		}

        echo json_encode($this->json);
    }

    private function delete_page()
    {
        $this->check_auth(true);

        $item = $this->admin_model->get_by_id('pages', 'id', $this->input->post('id'));

        if(empty($item)) {
            $this->json['status'] = self::RECORD_NOT_FOUND;
        } else {
            $its = $this->admin_model->get_all_by_id('pages_gallery', 'id_page', $item['id']);
            foreach ($its as $it)
                $this->inner_delete_image($it['file']);

            $this->admin_model->delete_by_id('pages', 'id', $item['id']);
        }

        echo json_encode($this->json);
    }

	/*:::Новости:::*/

	private function get_news()
	{
		$this->check_auth(true);

		$this->json['data'] = $this->admin_model->get_all('news');
		for ($i = 0; $i < count($this->json['data']); $i++) {

			$this->json['data'][$i]['publish'] = ($this->json['data'][$i]['publish'] == 1) ? true : false;

			$gallery = $this->admin_model->get_all_by_id('news_gallery', 'id_new', $this->json['data'][$i]['id']);
			$this->json['data'][$i]['gallery'] = array();
			foreach ($gallery as $image)
				$this->json['data'][$i]['gallery'][] = $this->get_image_links($image['file']);
		}

		echo json_encode($this->json);
	}

	private function get_new()
	{
		$this->check_auth(true);

		$item = $this->admin_model->get_by_id('news', 'id', $this->input->post('id'));
		if(empty($item)) {
			$this->json['status'] = self::RECORD_NOT_FOUND;
			exit(json_encode($this->json));
		}

		$this->json['data'] = $item;

		$this->json['data']['publish'] = ($item['publish'] == 1) ? true : false;

		$gallery = $this->admin_model->get_all_by_id('news_gallery', 'id_new', $this->json['data']['id']);
		$this->json['data']['gallery'] = array();
		foreach ($gallery as $image)
			$this->json['data']['gallery'][] = $this->get_image_links($image['file']);

		echo json_encode($this->json);
	}

	private function add_new()
	{
		$this->check_auth(true);

		$this->check_slug($this->input->post('slug'));

		$slug = $this->admin_model->get_by_id('news', 'slug', $this->input->post('slug'));
		if(!empty($slug)) {
			$this->json['status'] = self::SLUG_ALREADY_USED;
			exit(json_encode($this->json));
		}

		$insdata = array(
			'title' => $this->input->post('title'),
			'keywords' => $this->input->post('keywords'),
			'description' => $this->input->post('description'),
			'html' => $this->input->post('html'),
			'slug' => $this->input->post('slug'),
			'added' => $this->input->post('added'),
			'anchor' => $this->input->post('anchor'),
			'publish' => ($this->input->post('publish') == 'true') ? 1 : 0
		);

		$id = $this->admin_model->insert_data('news', $insdata);

		if($this->input->post('gallery') != NULL AND is_array($this->input->post('gallery'))) {
			foreach ($this->input->post('gallery') as $file) {
				$file_id = $this->handler_image(json_decode($file, true));
				if(!empty($file_id))
					$this->admin_model->insert_data('news_gallery', array('id_new' => $id, 'file' => $file_id));
			}
		}

		echo json_encode($this->json);
	}

	private function edit_new()
	{
		$this->check_auth(true);

		$item = $this->admin_model->get_by_id('news', 'id', $this->input->post('id'));

		if(empty($item)) {
			$this->json['status'] = self::RECORD_NOT_FOUND;
			exit(json_encode($this->json));
		}

		$this->check_slug($this->input->post('slug'));

		$slug = $this->admin_model->get_by_id('news', 'slug', $this->input->post('slug'));
		if(!empty($slug) AND $this->input->post('id') != $slug['id']) {
			$this->json['status'] = self::SLUG_ALREADY_USED;
			exit(json_encode($this->json));
		}

		$upddata = array(
			'title' => $this->input->post('title'),
			'keywords' => $this->input->post('keywords'),
			'description' => $this->input->post('description'),
			'html' => $this->input->post('html'),
			'slug' => $this->input->post('slug'),
			'added' => $this->input->post('added'),
			'anchor' => $this->input->post('anchor'),
			'publish' => ($this->input->post('publish') == 'true') ? 1 : 0
		);

		$this->admin_model->update_by_id('news', 'id', $item['id'], $upddata);

		$this->admin_model->delete_by_id('news_gallery', 'id_new', $item['id']);
		if($this->input->post('gallery') != NULL AND is_array($this->input->post('gallery'))) {
			foreach ($this->input->post('gallery') as $file) {
				$file_id = $this->handler_image(json_decode($file, true));
				if(!empty($file_id))
					$this->admin_model->insert_data('news_gallery', array('id_new' => $item['id'], 'file' => $file_id));
			}
		}

		echo json_encode($this->json);
	}

	private function delete_new()
	{
		$this->check_auth(true);

		$item = $this->admin_model->get_by_id('news', 'id', $this->input->post('id'));

		if(empty($item)) {
			$this->json['status'] = self::RECORD_NOT_FOUND;
		} else {
			$its = $this->admin_model->get_all_by_id('news_gallery', 'id_new', $item['id']);
			foreach ($its as $it)
				$this->inner_delete_image($it['file']);

			$this->admin_model->delete_by_id('news', 'id', $item['id']);
		}

		echo json_encode($this->json);
	}

	/*:::Статьи:::*/

	private function get_articles()
	{
		$this->check_auth(true);

		$this->json['data'] = $this->admin_model->get_all('articles');
		for ($i = 0; $i < count($this->json['data']); $i++) {

			$this->json['data'][$i]['publish'] = ($this->json['data'][$i]['publish'] == 1) ? true : false;

			if($this->json['data'][$i]['id_articles_section'] != NULL) {
				$this->json['data'][$i]['articles_section'] = $this->admin_model->get_by_id('articles_sections', 'id', $this->json['data'][$i]['id_articles_section']);
			} else {
				$this->json['data'][$i]['articles_section'] = array();
			}

			$gallery = $this->admin_model->get_all_by_id('articles_gallery', 'id_article', $this->json['data'][$i]['id']);
			$this->json['data'][$i]['gallery'] = array();
			foreach ($gallery as $image)
				$this->json['data'][$i]['gallery'][] = $this->get_image_links($image['file']);
		}

		echo json_encode($this->json);
	}

	private function get_article()
	{
		$this->check_auth(true);

		$item = $this->admin_model->get_by_id('articles', 'id', $this->input->post('id'));
		if(empty($item)) {
			$this->json['status'] = self::RECORD_NOT_FOUND;
			exit(json_encode($this->json));
		}

		$this->json['data'] = $item;

		$this->json['data']['publish'] = ($item['publish'] == 1) ? true : false;

		if($this->json['data']['id_articles_section'] != NULL) {
			$this->json['data']['articles_section'] = $this->admin_model->get_by_id('articles_sections', 'id', $this->json['data']['id_articles_section']);
		} else {
			$this->json['data']['articles_section'] = array();
		}

		$gallery = $this->admin_model->get_all_by_id('articles_gallery', 'id_article', $this->json['data']['id']);
		$this->json['data']['gallery'] = array();
		foreach ($gallery as $image)
			$this->json['data']['gallery'][] = $this->get_image_links($image['file']);

		echo json_encode($this->json);
	}

	private function add_article()
	{
		$this->check_auth(true);

		$this->check_slug($this->input->post('slug'));

		$slug = $this->admin_model->get_by_id('articles', 'slug', $this->input->post('slug'));
		if(!empty($slug)) {
			$this->json['status'] = self::SLUG_ALREADY_USED;
			exit(json_encode($this->json));
		}

		$insdata = array(
			'title' => $this->input->post('title'),
			'keywords' => $this->input->post('keywords'),
			'description' => $this->input->post('description'),
			'id_articles_section' => (empty($this->input->post('id_articles_section')) OR $this->input->post('id_articles_section') == 'null') ? null : $this->input->post('id_articles_section'),
			'html' => $this->input->post('html'),
			'slug' => $this->input->post('slug'),
			'added' => $this->input->post('added'),
			'anchor' => $this->input->post('anchor'),
			'publish' => ($this->input->post('publish') == 'true') ? 1 : 0
		);

		$id = $this->admin_model->insert_data('articles', $insdata);

		if($this->input->post('gallery') != NULL AND is_array($this->input->post('gallery'))) {
			foreach ($this->input->post('gallery') as $file) {
				$file_id = $this->handler_image(json_decode($file, true));
				if(!empty($file_id))
					$this->admin_model->insert_data('articles_gallery', array('id_article' => $id, 'file' => $file_id));
			}
		}

		echo json_encode($this->json);
	}

	private function edit_article()
	{
		$this->check_auth(true);

		$item = $this->admin_model->get_by_id('articles', 'id', $this->input->post('id'));

		if(empty($item)) {
			$this->json['status'] = self::RECORD_NOT_FOUND;
			exit(json_encode($this->json));
		}

		$this->check_slug($this->input->post('slug'));

		$slug = $this->admin_model->get_by_id('articles', 'slug', $this->input->post('slug'));
		if(!empty($slug) AND $this->input->post('id') != $slug['id']) {
			$this->json['status'] = self::SLUG_ALREADY_USED;
			exit(json_encode($this->json));
		}

		$upddata = array(
			'title' => $this->input->post('title'),
			'keywords' => $this->input->post('keywords'),
			'description' => $this->input->post('description'),
			'id_articles_section' => (empty($this->input->post('id_articles_section')) OR $this->input->post('id_articles_section') == 'null') ? null : $this->input->post('id_articles_section'),
			'html' => $this->input->post('html'),
			'slug' => $this->input->post('slug'),
			'added' => $this->input->post('added'),
			'anchor' => $this->input->post('anchor'),
			'publish' => ($this->input->post('publish') == 'true') ? 1 : 0
		);

		$this->admin_model->update_by_id('articles', 'id', $item['id'], $upddata);

		$this->admin_model->delete_by_id('articles_gallery', 'id_article', $item['id']);
		if($this->input->post('gallery') != NULL AND is_array($this->input->post('gallery'))) {
			foreach ($this->input->post('gallery') as $file) {
				$file_id = $this->handler_image(json_decode($file, true));
				if(!empty($file_id))
					$this->admin_model->insert_data('articles_gallery', array('id_article' => $item['id'], 'file' => $file_id));
			}
		}

		echo json_encode($this->json);
	}

	private function delete_article()
	{
		$this->check_auth(true);

		$item = $this->admin_model->get_by_id('articles', 'id', $this->input->post('id'));

		if(empty($item)) {
			$this->json['status'] = self::RECORD_NOT_FOUND;
		} else {
			$its = $this->admin_model->get_all_by_id('articles_gallery', 'id_article', $item['id']);
			foreach ($its as $it)
				$this->inner_delete_image($it['file']);

			$this->admin_model->delete_by_id('articles', 'id', $item['id']);
		}

		echo json_encode($this->json);
	}

	/*:::Разделы статей:::*/

	private function get_articles_sections()
	{
		$this->check_auth(true);

		$this->json['data'] = $this->admin_model->get_all('articles_sections');

		echo json_encode($this->json);
	}

	private function get_articles_section()
	{
		$this->check_auth(true);

		$item = $this->admin_model->get_by_id('articles_sections', 'id', $this->input->post('id'));
		if(empty($item)) {
			$this->json['status'] = self::RECORD_NOT_FOUND;
			exit(json_encode($this->json));
		}

		$this->json['data'] = $item;

		echo json_encode($this->json);
	}

	private function add_articles_section()
	{
		$this->check_auth(true);

		$this->check_slug($this->input->post('slug'));

		$slug = $this->admin_model->get_by_id('articles_sections', 'slug', $this->input->post('slug'));
		if(!empty($slug)) {
			$this->json['status'] = self::SLUG_ALREADY_USED;
			exit(json_encode($this->json));
		}

		$insdata = array(
			'slug' => $this->input->post('slug'),
			'name' => $this->input->post('name')
		);

		$id = $this->admin_model->insert_data('articles_sections', $insdata);

		echo json_encode($this->json);
	}

	private function edit_articles_section()
	{
		$this->check_auth(true);

		$item = $this->admin_model->get_by_id('articles_sections', 'id', $this->input->post('id'));

		if(empty($item)) {
			$this->json['status'] = self::RECORD_NOT_FOUND;
			exit(json_encode($this->json));
		}

		$this->check_slug($this->input->post('slug'));

		$slug = $this->admin_model->get_by_id('articles_sections', 'slug', $this->input->post('slug'));
		if(!empty($slug) AND $this->input->post('id') != $slug['id']) {
			$this->json['status'] = self::SLUG_ALREADY_USED;
			exit(json_encode($this->json));
		}

		$upddata = array(
			'slug' => $this->input->post('slug'),
			'name' => $this->input->post('name')
		);

		$this->admin_model->update_by_id('articles_sections', 'id', $item['id'], $upddata);

		echo json_encode($this->json);
	}

	private function delete_articles_section()
	{
		$this->check_auth(true);

		$item = $this->admin_model->get_by_id('articles_sections', 'id', $this->input->post('id'));

		if(empty($item)) {
			$this->json['status'] = self::RECORD_NOT_FOUND;
		} else {
			$this->admin_model->delete_by_id('articles_sections', 'id', $item['id']);
		}

		echo json_encode($this->json);
	}

	/*:::Инициализированные страницы:::*/

	private function get_def_pages()
	{
		$this->check_auth(true);

		$this->json['data'] = $this->admin_model->get_all('def_pages');

		echo json_encode($this->json);
	}

	private function get_short_def_pages()
	{
		$this->check_auth(true);

		$this->json['data'] = $this->admin_model->get_short_def_pages();

		echo json_encode($this->json);
	}

	private function get_def_page()
	{
		$this->check_auth(true);

		$item = $this->admin_model->get_by_id('def_pages', 'id', $this->input->post('id'));
		if(empty($item)) {
			$this->json['status'] = self::RECORD_NOT_FOUND;
			exit(json_encode($this->json));
		}

		$this->json['data'] = $item;

		echo json_encode($this->json);
	}

	private function get_short_def_page()
	{
		$this->check_auth(true);

		$item = $this->admin_model->get_short_def_page($this->input->post('id'));
		if(empty($item)) {
			$this->json['status'] = self::RECORD_NOT_FOUND;
			exit(json_encode($this->json));
		}

		$this->json['data'] = $item;

		echo json_encode($this->json);
	}

	private function edit_def_page()
	{
		$this->check_auth(true);

		$item = $this->admin_model->get_by_id('def_pages', 'id', $this->input->post('id'));

		if(empty($item)) {
			$this->json['status'] = self::RECORD_NOT_FOUND;
			exit(json_encode($this->json));
		}

		$upddata = array(
			'title' => $this->input->post('title'),
			'keywords' => $this->input->post('keywords'),
			'description' => $this->input->post('description'),
			'html' => $this->input->post('html')
		);

		$this->admin_model->update_by_id('def_pages', 'id', $item['id'], $upddata);

		echo json_encode($this->json);
	}
}
