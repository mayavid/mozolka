<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

	private $data = array();

	private $helper_section;

	function __construct() {
		parent::__construct();

		$this->data['title'] = "";
		$this->data['keywords'] = "";
		$this->data['description'] = "";

		$this->load->model('site_model');
	}

	public function index()
	{
		if(isset($_POST['search'])) {
			redirect(base_url().'search/result?query='.base64_encode($this->input->post('search', TRUE)));
		} else {
			redirect(base_url());
		}
	}

	public function result() {
		$this->data['title'] = "Результат поиска";
		$this->data['keywords'] = "Результат поиска";
		$this->data['description'] = "Результат поиска";

		$str = base64_decode($this->input->get('query',TRUE));

		$result = array();
		$news = $this->site_model->search_news($str);
		foreach ($news as $item) {
			$item['what'] = "Новость";
			$result[] = $item;
		}
		$articles = $this->site_model->search_articles($str);
		foreach ($articles as $item) {
			$item['what'] = "Статья";
			$result[] = $item;
		}
		$pages = $this->site_model->search_pages($str);
		foreach ($pages as $item) {
			$item['what'] = "Страница";
			$result[] = $item;
		}

		usort($result, function($a, $b) {
			return $a['id'] - $b['id'];
		});

		$total = count($result);
		$limit = 5;

		$this->load->library('pagination');
		$this->load->helper('utilites_helper');

		$config = get_pagination_config(base_url()."search/result", $total, $limit);

		$this->pagination->initialize($config);
		$this->data['pagination'] = $this->pagination->create_links();

		$result = array_slice($result, (isset($_GET['page'])) ? ($limit*$_GET['page'] - $limit) : 0, $limit);
		for($i = 0; $i < count($result); $i++) {
			unset($result[$i]['html']);
			if($result[$i]['what'] == 'Новость') {
				$result[$i]['link'] = base_url()."news/".$result[$i]['slug'];
			}
			if($result[$i]['what'] == 'Статья') {
				$section = $this->site_model->get_by_id('articles_sections', 'id', $result[$i]['id_articles_section']);
				$result[$i]['link'] = base_url()."articles/".$section['slug']."/".$result[$i]['slug'];
			}
			if($result[$i]['what'] == 'Страница') {
				$result[$i]['link'] = base_url()."uslugi/";
				$sections = array();
				$this->helper_section = $this->site_model->get_by_id('sections', 'id_page', $result[$i]['id']);
				$sections[] = $this->helper_section['slug'];

				while($this->helper_section['parent'] != 0) {
					$this->helper_section = $this->site_model->get_by_id('sections', 'id', $this->helper_section['parent']);
					$sections[] = $this->helper_section['slug'];
				}

				foreach (array_reverse($sections) as $slug) {
					$result[$i]['link'] .= $slug."/";
				}

				$result[$i]['link'] = rtrim($result[$i]['link'], "/");
			}
		}

		$this->data['result'] = $result;

		$part1 = (isset($_GET['page'])) ? ($limit*$_GET['page'] - $limit + 1) : 1;
		$part2 = (($part1 - 1 + $limit) > $total) ? $total : ($part1 - 1 + $limit);

		$this->data['search_note'] = $part1. " - " .$part2. " из ".$total;

		$this->display_lib->display_page($this->data, "content/search_result");
	}
}
