<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Функция вывода массива в удобочитаемой форме
 * @param $array
 */
function print_arr($array)
{
    echo "<pre>" . print_r($array, true) . "</pre>";
}

/**
 * Функция генерирует uuid
 * @return string
 */
function generate_uuid_v4() {
    return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

        // 32 bits for "time_low"
        mt_rand(0, 0xffff), mt_rand(0, 0xffff),

        // 16 bits for "time_mid"
        mt_rand(0, 0xffff),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand(0, 0x0fff) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand(0, 0x3fff) | 0x8000,

        // 48 bits for "node"
        mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
}

/**
 * Подготовка дерева каталога
 * @param $dataset
 * @return array
 */
function prepare_tree($dataset)
{
    $tree = array();
    foreach ($dataset as $data) {
        $tree[$data['id']] = $data;
    }
    return $tree;
}

/**
 * Постоение дерева
 * @param $dataset
 * @return array
 */
function map_tree($dataset)
{
    $dataset = prepare_tree($dataset);

    $tree = array();

    foreach ($dataset as $id => &$node) {
        if (empty($node['parent'])) {
            $tree[$id] = &$node;
        } else {
            $dataset[$node['parent']]['childs'][$id] = &$node;
        }
    }

    return $tree;
}

/**
 * Получение ID дочерних категорий
 **/
function cats_id($array, $id){
    if(!$id) return false;

    $data = '';
    foreach($array as $item){
        if($item['parent'] == $id){
            $data .= $item['id'] . ",";
            $data .= cats_id($array, $item['id']);
        }
    }
    return $data;
}


function get_image_link($file, $thumb = true) {
	if(!empty($file)) {
		$DIR_IMAGES	= "./assets/uploads/images/";
		$DIR_THUMBS	= "./assets/uploads/thumbs/";

		$d1 = mb_substr($file, 0, 2);
		$d2 = mb_substr($file, 2, 2);

		$md5_image = md5_file($DIR_IMAGES .$d1."/".$d2."/". $file);
		$md5_thumb = md5_file($DIR_THUMBS .$d1."/".$d2."/". $file);

		if($thumb)
			return base_url() . mb_substr($DIR_THUMBS, 2) .$d1."/".$d2."/". $file."?".$md5_thumb;
		else
			return base_url() . mb_substr($DIR_IMAGES, 2) .$d1."/".$d2."/". $file."?".$md5_image;
	}

	return null;
}

function get_pagination_config($base_url, $total, $limit) {
	$config = array();
	$config['base_url'] = $base_url;
	$config['total_rows'] = $total;
	$config['use_page_numbers'] = TRUE;
	$config['page_query_string'] = TRUE;
	$config['query_string_segment'] = 'page';
	$config['per_page'] = $limit;
	$config['reuse_query_string'] = TRUE;

	$config['full_tag_open'] = '<div class="paginator__list">';
	$config['full_tag_close'] = '</div>';
	$config['first_tag_open'] = '<div class="paginator__item">';
	$config['first_tag_close'] = '</div>';
	$config['last_tag_open'] = '<div class="paginator__item">';
	$config['last_tag_close'] = '</div>';
	$config['next_tag_open'] = '<div class="paginator__item">';
	$config['next_tag_close'] = '</div>';
	$config['prev_tag_open'] = '<div class="paginator__item">';
	$config['prev_tag_close'] = '</div>';
	$config['cur_tag_open'] = '<div class="paginator__item is-active"><a href="#">';
	$config['cur_tag_close'] = '</a></div>';
	$config['num_tag_open'] = '<div class="paginator__item">';
	$config['num_tag_close'] = '</div>';
	$config['first_link'] = 'Начало';
	$config['last_link'] = 'Конец';
	$config['next_link'] = 'Далее';
	$config['prev_link'] = 'Назад';

	return $config;
}

function encryptDecrypt($input) {
	$key = ['b', 'I', '-', 'P', 'i']; //Тут могут быть любые символы и в любом количестве
	$output = '';
	for ($i = 0; $i < strlen($input); $i++) {
		$output .= $input[$i] ^ $key[$i % count($key)];
	}
	return $output;
}

function getReCaptchaV3($secretKey){
	$key = "6Lc4teEUAAAAAHC7GYlL-cKkyUXPI79y7KfdrZvQ";
	$response = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$key.'&response='.$secretKey);
	return json_decode($response);
}