<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Display_lib
{
	private $CI;

	function __construct() {

		$this->CI = &get_instance();

		$this->CI->load->helper('utilites_helper');

		$this->CI->load->model('crud_model');
		$this->CI->load->model('site_model');

		if (!$this->CI->session->has_userdata('city')) {
			$this->CI->session->set_userdata('city', 'spb');
		}
	}

    public function display_page($data, $content_view)
    {
    	$data['main_menu'] = $this->get_menu();
		$data['bottom_sections'] = $this->get_bottom_sections();

		$this->CI->load->view('cake/preheader', $data);
		$this->CI->load->view('cake/header', $data);
		$this->CI->load->view($content_view, $data);
		$this->CI->load->view('cake/footer', $data);
    }

    public function display_404()
    {
		$data['title'] = "404 - Страница не найдена";
		$data['keywords'] = "404 - Страница не найдена";
		$data['description'] = "404 - Страница не найдена";

		$data['main_menu'] = $this->get_menu();
		$data['bottom_sections'] = $this->get_bottom_sections();

		$this->CI->load->view('cake/preheader', $data);
		$this->CI->load->view('cake/header', $data);
		$this->CI->load->view("content/error_404");
		$this->CI->load->view('cake/footer', $data);
    }

    private function get_menu()
	{
		$sections = $this->CI->site_model->get_all_sort('sections', 'rating', 'DESC');

		$tree = map_tree($sections);

		return $tree;
	}

	private function get_bottom_sections()
	{
		return $this->CI->site_model->get_sections_for_bottom();
	}
}