<footer class="footer">
	<div class="footer__map">
		<div class="footer__map-layout"></div>
	</div>
	<div class="footer__top">
		<div class="inner">
			<div class="footer__head">
				<div class="footer__head-logo"><a href="<?=base_url()?>"><img src="<?=base_url()?>assets/img/logo-white.svg"></a></div>
				<div class="footer__head-text">Узнавайте о новостях первым</div>
				<div class="footer__head-search">
					<form class="form form_ajax" action="<?=base_url()?>forms/subscribe" method="post" novalidate autocomplete="off">
						<div class="form__wrap">
							<div class="form__field form__field_search">
								<div class="form__input">
									<input type="email" name="email" data-require="email" placeholder="Ваш E-mail">
								</div>
							</div>
							<div class="form__field form__field_submit">
								<div class="form__input">
									<button class="btn btn_red btn_fill" type="submit"><span><i class="fa fa-paper-plane"></i></span></button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="footer__head-soc">
					<div class="footer__head-soc-title">Мы в Соц. сетях</div>
					<div class="footer__head-soc-list"><a class="footer__head-soc-item" href="#"><i class="fa fa-facebook-square"></i></a><a class="footer__head-soc-item" href="#"><i class="fa fa-vk"></i></a></div>
				</div>
			</div>
			<div class="footer__nav">
				<div class="footer__title">Услуги</div>
				<div class="footer__nav-list">
					<?foreach ($bottom_sections as $item):?>
						<div class="footer__nav-item"><a class="footer__nav-link" href="<?=base_url()?>uslugi/<?=$item['slug']?>"><?=$item['name']?></a></div>
					<?endforeach;?>
				</div>
			</div>
			<div class="footer__contact">
				<div class="footer__title">Контакты</div>
				<div class="footer__contact-list">
					<div class="footer__contact-item">
						<div class="footer__contact-icon"><i class="fa fa-map-marker"></i></div>
						<div class="footer__contact-text">СПБ, пл. Александра Невского, д. 2., БЦ «Москва», офис 1008, 1009</div>
					</div>
					<div class="footer__contact-item">
						<div class="footer__contact-icon"><i class="fa fa-map-marker"></i></div>
						<div class="footer__contact-text">Москва, пл. Александра Невского, д. 2., БЦ «Москва», офис 1008, 1009</div>
					</div>
					<div class="footer__contact-item">
						<div class="footer__contact-icon"><i class="fa fa-phone"></i></div>
						<div class="footer__contact-text">8 800 600 26 96</div>
					</div>
					<div class="footer__contact-item">
						<div class="footer__contact-icon"><i class="fa fa-envelope"></i></div>
						<div class="footer__contact-text"><a href="mailto:mailforcompany@mail.ru">mailforcompany@mail.ru</a></div>
					</div>
					<div class="footer__contact-item">
						<div class="footer__contact-icon"><i class="fa fa-clock-o"></i></div>
						<div class="footer__contact-text">Пн. - Пт. 9:00 - 18:00</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer__info">
		<div class="inner">
			<div class="footer__info-item"><a href="<?=base_url()?>privacy_policy">Политика конфиденциальности</a></div>
			<div class="footer__info-item">2019</div>
			<div class="footer__info-item">© группа компаний «БиПИ»</div>
		</div>
	</div>
</footer>
<div class="popup popup_callback">
	<div class="popup__overlay"></div>
	<div class="popup__inner">
		<div class="popup__layout">
			<div class="popup__close"></div>
			<div class="popup__content">
				<div class="popup__title">Заказать звонок</div>
				<div class="popup__text">
					<p>Укажите свой контактный номер телефона, и мы свяжемся с Вами в ближайшее время.</p>
				</div>
				<div class="popup__form">
					<form class="form form_ajax" action="<?=base_url()?>forms/order_call" method="post" novalidate autocomplete="off">
						<div class="form__wrap">
							<div class="form__field">
								<div class="form__input">
									<input type="text" name="fio" data-require="true" placeholder="Ваше имя">
								</div>
							</div>
							<div class="form__field">
								<div class="form__input">
									<input type="text" name="phone" data-require="true" data-mask="+7 (999) 999-99-99" placeholder="Ваш номер телефона">
								</div>
							</div>
							<div class="form__field">
								<div class="form__input">
									<label>
										<input type="checkbox" name="confirm" data-require="true">
										<div class="form__label">Соглашаюсь на <a href="<?=base_url()?>personal" target="_blank">обработку персональных данных</a></div>
									</label>
								</div>
							</div>
							<div class="form__field form__field_submit">
								<div class="form__input">
									<button class="btn btn_red btn_fill" type="submit"><span>Перезвоните мне</span></button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="popup popup_order">
	<div class="popup__overlay"></div>
	<div class="popup__inner">
		<div class="popup__layout">
			<div class="popup__close"></div>
			<div class="popup__content">
				<div class="popup__title">Заказать услугу</div>
				<div class="popup__form">
					<form class="form form_ajax" action="<?=base_url()?>forms/order_service" method="post" novalidate autocomplete="off" enctype="multipart/form-data">
						<div class="form__wrap">
							<div class="form__field">
								<div class="form__input">
									<input type="text" name="fio" data-require="true" placeholder="Ваше имя">
								</div>
							</div>
							<div class="form__field form__field_2">
								<div class="form__input">
									<input type="text" name="phone" data-require="true" data-mask="+7 (999) 999-99-99" placeholder="Ваш номер телефона">
								</div>
							</div>
							<div class="form__field form__field_2">
								<div class="form__input">
									<input type="email" name="email" data-require="true" placeholder="Ваш e-mail">
								</div>
							</div>
							<div class="form__field">
								<div class="form__input">
									<textarea name="message" data-require="true" placeholder="Комментарий к заявке*"></textarea>
								</div>
							</div>
							<div class="form__field">
								<div class="form__input">
									<label>
										<input type="file" name="files[]" accept=".png,.jpg,.tif,.pdf,.doc,.docs" multiple>
										<div class="form__label">Прикрепить свои файлы</div>
									</label>
								</div>
							</div>
							<div class="form__field">
								<div class="form__input">
									<label>
										<input type="checkbox" name="confirm" data-require="true" checked>
										<div class="form__label">Соглашаюсь на <a href="<?=base_url()?>personal" target="_blank">обработку персональных данных</a></div>
									</label>
								</div>
							</div>
							<div class="form__field form__field_submit">
								<div class="form__input">
									<button class="btn btn_red btn_fill" type="submit"><span>Отправить заявку</span></button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=''"></script>
<script src="https://www.google.com/recaptcha/api.js?render=6Lc4teEUAAAAAIaSeXfPxjj79ms6vhMs7WEL7gHY"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/main.js?034298da0dad11e2a8c1"></script></body>
</html>