<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title><?=$title;?></title>
	<meta name="description" content="<?=htmlspecialchars($description, ENT_QUOTES, 'UTF-8');?>">
	<meta name="keywords" content="<?=htmlspecialchars($keywords, ENT_QUOTES, 'UTF-8');?>">
	<meta property="og:title" content="<?=htmlspecialchars($title, ENT_QUOTES, 'UTF-8');?>"/>
	<meta property="og:description" content="<?=htmlspecialchars($description, ENT_QUOTES, 'UTF-8');?>"/>
	<meta property="og:image" content="/img.jpg"/>
	<meta property="og:url" content="{{page url}}"/>
	<meta name="twitter:title" content="<?=htmlspecialchars($title, ENT_QUOTES, 'UTF-8');?>"/>
	<meta name="twitter:description" content="<?=htmlspecialchars($description, ENT_QUOTES, 'UTF-8');?>"/>
	<meta name="twitter:image" content="/img.jpg"/>
	<meta name="twitter:card" content="summary_large_image"/>
	<link rel="icon" type="image/x-icon" href="/favicon.ico">
	<link href="<?=base_url()?>assets/css/main.css?034298da0dad11e2a8c1" rel="stylesheet">
</head>