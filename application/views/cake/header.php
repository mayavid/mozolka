<body>
<div class="wrapper">
	<header class="header">
		<div class="header__menu">
			<div class="header__menu-inner">
				<div class="inner">
					<div class="header__city">
						<div class="header__city-title">Ваш город:</div>
						<div class="header__city-value"><?=($this->session->userdata('city') == "spb") ? "Санкт-Петербург" : "Москва";?></div>
						<div class="header__city-list">
<!--							<div class="header__city-item"><a href="--><?//=base_url()?><!--welcome/change_city/spb">Санкт-Петербург</a></div>-->
<!--							<div class="header__city-item"><a href="--><?//=base_url()?><!--welcome/change_city/msk">Москва</a></div>-->
							<div class="header__city-item">Санкт-Петербург</div>
							<div class="header__city-item">Москва</div>
						</div>
					</div>
					<div class="header__nav">
						<div class="header__nav-item"><a href="<?=base_url()?>about">О&nbsp;компании</a></div>
						<div class="header__nav-item"><a href="<?=base_url()?>about/employees">Наша команда</a></div>
						<div class="header__nav-item"><a href="<?=base_url()?>about/feedback">Отзывы</a></div>
						<div class="header__nav-item"><a href="<?=base_url()?>articles"">Статьи</a></div>
						<div class="header__nav-item"><a href="<?=base_url()?>news">Новости</a></div>
						<div class="header__nav-item"><a href="<?=base_url()?>contacts">Контакты</a></div>
					</div>
				</div>
			</div>
		</div>
		<div class="header__body">
			<div class="inner">
				<div class="header__logo"><a class="header__logo-item" href="<?=base_url()?>"><img src="<?=base_url()?>assets/img/logo.svg"></a></div>
				<?if($this->session->userdata('city') == "spb") {?>
					<div class="header__icon"><img src="<?=base_url()?>assets/img/city-spb.svg"></div>
				<?}?>
				<?if($this->session->userdata('city') == "msk") {?>
					<div class="header__icon"><img src="<?=base_url()?>assets/img/city-msk.svg"></div>
				<?}?>
				<div class="header__action">
					<div class="header__action-phones">
						<?if($this->session->userdata('city') == "spb"){?>
							<a class="header__action-phones-item" href="tel:88002508647">8 (800) 250-86-47</a>
							<a class="header__action-phones-item" href="tel:88126005178">8 (812) 600-51-78</a>
						<?}?>
						<?if($this->session->userdata('city') == "msk"){?>
							<a class="header__action-phones-item" href="tel:88002508647">8 (800) 250-86-47</a>
							<a class="header__action-phones-item" href="tel:84995044126">8 (499) 504-41-26</a>
						<?}?>
					</div>
					<div class="header__action-case"><a class="header__action-case-item" href="<?=base_url()?>calculator">
							<div class="header__action-case-icon"><img src="<?=base_url()?>assets/img/icon-calc.svg"></div>
							<div class="header__action-case-text">Расчет <br>стоимости</div></a><a class="header__action-case-item" href="#" data-popup-open=".popup_order">
							<div class="header__action-case-icon"><img src="<?=base_url()?>assets/img/icon-doc.svg"></div>
							<div class="header__action-case-text">Заказать <br>услугу</div></a></div>
					<div class="header__action-call">
						<div class="btn btn_red btn_fill" data-popup-open=".popup_callback"><span>Обратный звонок</span></div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<nav class="nav nav_wide">
		<div class="nav__inner">
			<div class="inner">
				<div class="nav__menu">
					<div class="nav__menu-inner">
						<form class="nav__menu-search" action="<?=base_url()?>search" method="post">
							<input type="text" name="search" data-require="true">
							<button class="fa fa-search" type="submit"></button>
						</form>
						<div class="nav__menu-wrap">
							<div class="nav__menu-line">
								<div class="nav__menu-list">
									<?foreach ($main_menu as $m1):?>
										<?if($m1['publish'] == 1) {?>
											<div class="nav__menu-item">
												<?if($m1['id_page'] != null) {?>
													<a class="nav__menu-link nav__menu-link_inset nav__menu-link_upper" href="<?=base_url()."uslugi/".$m1['slug'];?>"><?=$m1['name']?></a>
												<?} else {?>
													<p class="nav__menu-link nav__menu-link_inset nav__menu-link_upper"><?=$m1['name']?></p>
												<?}?>
												<?if(isset($m1['childs'])) {?>
													<div class="nav__menu-sub">
														<?foreach ($m1['childs'] as $m2):?>
															<?if($m2['publish'] == 1) {?>
																<div class="nav__menu-item">
																	<?if($m2['id_page'] != null) {?>
																		<a class="nav__menu-link nav__menu-link_inset" href="<?=base_url()."uslugi/".$m1['slug']."/".$m2['slug'];?>"><?=$m2['name']?></a>
																	<?} else {?>
																		<p class="nav__menu-link nav__menu-link_inset"><?=$m2['name']?></p>
																	<?}?>
																	<?if(isset($m2['childs'])) {?>
																		<div class="nav__menu-sub">
																			<?foreach ($m2['childs'] as $m3):?>
																				<?if($m3['publish'] == 1) {?>
																					<div class="nav__menu-item">
																						<?if($m3['id_page'] != null) {?>
																							<a class="nav__menu-link" href="<?=base_url()."uslugi/".$m1['slug']."/".$m2['slug']."/".$m3['slug'];?>"><?=$m3['name']?></a>
																						<?} else {?>
																							<p class="nav__menu-link"><?=$m3['name']?></p>
																						<?}?>
																					</div>
																				<?}?>
																			<?endforeach;?>
																		</div>
																	<?}?>
																</div>
															<?}?>
														<?endforeach;?>
													</div>
												<?}?>
											</div>
										<?}?>
									<?endforeach;?>
								</div>
							</div>
							<div class="nav__menu-arrow nav__menu-arrow_prev">
								<div class="fa fa-chevron-left"></div>
							</div>
							<div class="nav__menu-arrow nav__menu-arrow_next">
								<div class="fa fa-chevron-right"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="nav__submenu">
				<div class="nav__submenu-overlay"></div>
				<div class="nav__submenu-wrap">
					<div class="inner">
						<div class="nav__submenu-list scrollbar">
							<div class="nav__submenu-inner">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</nav>
	<nav class="nav nav_compact">
		<div class="nav__handler"><i></i></div>
		<div class="nav__inner">
			<div class="nav__head">
				<div class="nav__city">
					<div class="nav__city-title">Ваш город:</div>
					<div class="nav__city-value"><?=($this->session->userdata('city') == "spb") ? "Санкт-Петербург" : "Москва";?></div>
					<div class="nav__city-list">
<!--						<div class="nav__city-item"><a href="--><?//=base_url()?><!--welcome/change_city/spb">Санкт-Петербург</a></div>-->
<!--						<div class="nav__city-item"><a href="--><?//=base_url()?><!--welcome/change_city/msk">Москва</a></div>-->
						<div class="nav__city-item">Санкт-Петербург</div>
						<div class="nav__city-item">Москва</div>
					</div>
				</div>
				<?if($this->session->userdata('city') == "spb") {?>
					<div class="nav__city-ico"><img src="<?=base_url()?>assets/img/city-spb.svg"></div>
				<?}?>
				<?if($this->session->userdata('city') == "msk") {?>
					<div class="nav__city-ico"><img src="<?=base_url()?>assets/img/city-msk.svg"></div>
				<?}?>
				<a class="nav__call" href="#" data-popup-open=".popup_callback">
					<div class="btn btn_fill btn_red"><span><i class="fa fa-phone"></i></span></div>
				</a>
			</div>
			<div class="nav__menu">
				<div class="nav__menu-inner">
					<div class="nav__group nav__group_service">
						<div class="nav__menu-title">Услуги</div>
						<div class="nav__menu-list">
							<?foreach ($main_menu as $m1):?>
								<?if($m1['publish'] == 1) {?>
									<div class="nav__menu-item">
										<?if($m1['id_page'] != null) {?>
											<a class="nav__menu-link nav__menu-link_inset nav__menu-link_upper" href="<?=base_url()."uslugi/".$m1['slug'];?>"><?=$m1['name']?></a>
										<?} else {?>
											<p class="nav__menu-link nav__menu-link_inset nav__menu-link_upper"><?=$m1['name']?></p>
										<?}?>
										<?if(isset($m1['childs'])) {?>
											<div class="nav__menu-sub">
												<?foreach ($m1['childs'] as $m2):?>
													<?if($m2['publish'] == 1) {?>
														<div class="nav__menu-item">
															<?if($m2['id_page'] != null) {?>
																<a class="nav__menu-link nav__menu-link_inset" href="<?=base_url()."uslugi/".$m1['slug']."/".$m2['slug'];?>"><?=$m2['name']?></a>
															<?} else {?>
																<p class="nav__menu-link nav__menu-link_inset"><?=$m2['name']?></p>
															<?}?>
															<?if(isset($m2['childs'])) {?>
																<div class="nav__menu-sub">
																	<?foreach ($m2['childs'] as $m3):?>
																		<?if($m3['publish'] == 1) {?>
																			<div class="nav__menu-item">
																				<?if($m3['id_page'] != null) {?>
																					<a class="nav__menu-link" href="<?=base_url()."uslugi/".$m1['slug']."/".$m2['slug']."/".$m3['slug'];?>"><?=$m3['name']?></a>
																				<?} else {?>
																					<p class="nav__menu-link"><?=$m3['name']?></p>
																				<?}?>
																			</div>
																		<?}?>
																	<?endforeach;?>
																</div>
															<?}?>
														</div>
													<?}?>
												<?endforeach;?>
											</div>
										<?}?>
									</div>
								<?}?>
							<?endforeach;?>
						</div>
					</div>
					<div class="nav__group nav__group_more">
						<div class="nav__menu-list">
							<div class="nav__menu-item"><a class="nav__menu-link" href="<?=base_url()?>about">О компании</a></div>
							<div class="nav__menu-item"><a class="nav__menu-link" href="<?=base_url()?>about/employees">Наша команда</a></div>
							<div class="nav__menu-item"><a class="nav__menu-link" href="<?=base_url()?>about/feedback">Отзывы</a></div>
							<div class="nav__menu-item"><a class="nav__menu-link" href="<?=base_url()?>articles">Статьи</a></div>
							<div class="nav__menu-item"><a class="nav__menu-link" href="<?=base_url()?>news">Новости</a></div>
							<div class="nav__menu-item"><a class="nav__menu-link" href="<?=base_url()?>contacts">Контакты</a></div>
						</div>
					</div>
					<div class="nav__group nav__group_action">
						<div class="nav__action"><a class="nav__action-item" href="#">
								<div class="nav__action-icon"><img src="<?=base_url()?>assets/img/icon-calc.svg"></div>
								<div class="nav__action-text">Расчет <br>стоимости</div></a><a class="nav__action-item" href="#">
								<div class="nav__action-icon"><img src="<?=base_url()?>assets/img/icon-doc.svg"></div>
								<div class="nav__action-text">Заказать <br>услугу</div></a></div>
					</div>
					<div class="nav__group nav__group_contact">
						<div class="nav__contact">
							<div class="nav__contact-icon"><i class="fa fa-map-marker"></i></div>
							<div class="nav__contact-text"><?=($this->session->userdata('city') == "spb") ? "СПБ, пл. Александра Невского, д. 2., БЦ «Москва», офис 1008, 1009" : "Москва, 3-й Угрешский проезд, д. 8А, стр.1, офис 1";?></div>
						</div>
						<div class="nav__contact">
							<div class="nav__contact-icon"><i class="fa fa-phone"></i></div>
							<div class="nav__contact-text">8 800 600 26 96</div>
						</div>
						<div class="nav__contact">
							<div class="nav__contact-icon"><i class="fa fa-envelope"></i></div>
							<div class="nav__contact-text">mailforcompany@mail.ru</div>
						</div>
						<div class="nav__contact">
							<div class="nav__contact-icon"><i class="fa fa-clock-o"></i></div>
							<div class="nav__contact-text">Пн. - Пт. 9:00 - 18:00</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</nav>