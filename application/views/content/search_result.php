<div class="website">
	<div class="article">
		<div class="inner">
			<div class="article__wrap">
				<div class="article__content">
					<div class="breadcrumbs"><a class="breadcrumbs__item" href="<?=base_url()?>">Главная</a>
						<div class="breadcrumbs__item">Результат поиска</div>
					</div>
					<div class="content">
						<h1>Результат поиска</h1>
						<?foreach ($result as $item):?>
							<div class="news-preview">
								<div class="news-preview__info">
									<div class="news-preview__title"><a href="<?=$item['link']?>"><?=$item['title']?></a></div>
									<div class="news-preview__date"><?=$item['what']?></div>
									<div class="news-preview__text"><?=$item['description']?></div>
									<div class="news-preview__more"><a class="btn btn_red btn_fill" href="<?=$item['link']?>"><span>Подробнее</span></a></div>
								</div>
							</div>
						<?endforeach;?>
						<?if(count($result) > 0) {?>
							<div class="paginator">
								<div class="paginator__title">Результат поиска <?=$search_note;?></div>
								<?=$pagination;?>
							</div>
						<?}?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>