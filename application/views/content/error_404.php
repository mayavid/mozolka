<div class="website">
	<div class="article">
		<div class="inner">
			<div class="article__wrap">
				<div class="article__aside">
					<div class="article__aside-stick">
						<div class="article__stick">
							<div class="downloadprice"><a class="downloadprice__inner" href="https://bi-pi.ru/images/price_list.pdf" target="_blank">
									<div class="downloadprice-icon"><i class="fa fa-file-pdf-o"></i></div>
									<div class="downloadprice-text">Скачать прайслист</div></a></div>
						</div>
					</div>
				</div>
				<div class="article__content">
					<div class="breadcrumbs"><a class="breadcrumbs__item" href="<?=base_url()?>">Главная</a>
						<div class="breadcrumbs__item">Страница не найдена</div>
					</div>
					<div class="content">
						<h1 class="bp__text-xxlarge bp__text-center">404</h1>
						<p class="bp__text-title bp__text-center">Страница не найдена</p>
						<p class="bp__text-center">Пожалуйста, перейдите на <a href="<?=base_url()?>">главную страницу</a>
							<div class="note bp__margin-top-xlarge">
						<p><img src="https://bi-pi.ru/images/bipi.png"></p>
						<p>Ни один электронный портал не заменит Вам живого общения со специалистом в области учета, анализа и отчетности.						</p>
					</div>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
</div>