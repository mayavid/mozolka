<div class="website">
	<div class="article">
		<div class="inner">
			<div class="article__wrap">
				<div class="article__content">
					<div class="breadcrumbs"><a class="breadcrumbs__item" href="<?=base_url()?>">Главная</a>
						<div class="breadcrumbs__item">Новости</div>
					</div>
					<div class="content">
						<h1>Новости</h1>
						<?foreach ($news as $item):?>
							<div class="news-preview">
								<div class="news-preview__image"><a href="<?=base_url()?>news/<?=$item['slug']?>"><img class="lazy" data-src="<?=$item['thumb']?>"></a></div>
								<div class="news-preview__info">
									<div class="news-preview__title"><a href="<?=base_url()?>news/<?=$item['slug']?>"><?=$item['anchor']?></a></div>
									<div class="news-preview__date"><?=$item['added']?></div>
									<div class="news-preview__text"><?=$item['description']?></div>
									<div class="news-preview__more"><a class="btn btn_red btn_fill" href="<?=base_url()?>news/<?=$item['slug']?>"><span>Подробнее</span></a></div>
								</div>
							</div>
						<?endforeach;?>
						<?if(count($news) > 0) {?>
							<div class="paginator">
								<div class="paginator__title">Новости <?=$news_note;?></div>
								<?=$pagination;?>
							</div>
						<?}?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>