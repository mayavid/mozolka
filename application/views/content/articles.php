<div class="website">
	<div class="article">
		<div class="inner">
			<div class="article__wrap">
				<div class="article__aside">
					<div class="article__aside-menu">
						<div class="article__menu">
							<div class="article__menu-list">
								<a class="article__menu-item <?=($this->uri->segment(2) == null) ? "is-active" : null;?>" href="<?=base_url()?>articles">Все разделы</a>
								<?foreach($sections as $item):?>
									<a class="article__menu-item <?=($this->uri->segment(2) == $item['slug']) ? "is-active" : null;?>" href="<?=base_url()?>articles/<?=$item['slug'];?>"><?=$item['name'];?><sup><small><?=$item['count'];?></small></sup></a>
								<?endforeach;?>
							</div>
						</div>
					</div>
				</div>
				<div class="article__content">
					<div class="breadcrumbs"><a class="breadcrumbs__item" href="<?=base_url()?>">Главная</a>
						<div class="breadcrumbs__item">Статьи</div>
					</div>
					<div class="content">
						<h1><?=$title;?></h1>
						<?foreach ($articles as $item):?>
							<div class="news-preview">
								<div class="news-preview__image"><a href="<?=base_url()?>articles/<?=$item['section']['slug']?>/<?=$item['slug']?>"><img class="lazy" data-src="<?=$item['thumb']?>"></a></div>
								<div class="news-preview__info">
									<div class="news-preview__title"><a href="<?=base_url()?>articles/<?=$item['section']['slug']?>/<?=$item['slug']?>"><?=$item['anchor']?></a></div>
									<div class="news-preview__date"><?=$item['added']?></div>
									<div class="news-preview__text"><?=$item['description']?></div>
									<div class="news-preview__more"><a class="btn btn_red btn_fill" href="<?=base_url()?>articles/<?=$item['section']['slug']?>/<?=$item['slug']?>"><span>Подробнее</span></a></div>
								</div>
							</div>
						<?endforeach;?>
						<?if(count($articles) > 0) {?>
							<div class="paginator">
								<div class="paginator__title">Статьи <?=$articles_note;?></div>
								<?=$pagination;?>
							</div>
						<?}?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>