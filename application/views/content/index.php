<div class="website">
	<div class="slider">
		<div class="slider__container">
			<div class="slider__wrap">
				<div class="slider__item">
					<div class="slider__item-background"><img class="img-fit" src="https://bi-pi.ru/upload/iblock/75a/75a9b44058928f425075d28572ce5d12.jpg"></div>
					<div class="inner">
						<div class="slider__item-content">
							<div class="slider__item-title">Экспресс- консультации для начинающих бизнесменов</div>
							<div class="slider__item-btn"><a class="btn btn_red" href="#"><span>Узнать подробнее</span></a></div>
						</div>
					</div>
				</div>
				<div class="slider__item">
					<div class="slider__item-background"><img class="img-fit" src="https://bi-pi.ru/upload/iblock/9c2/9c21044b5e955a78ac1dc89d9ac13e35.jpg"></div>
					<div class="inner">
						<div class="slider__item-content">
							<div class="slider__item-title">Бухгалтерский учет и аудит</div>
							<div class="slider__item-btn"><a class="btn btn_red" href="#"><span>Узнать подробнее</span></a></div>
						</div>
					</div>
				</div>
				<div class="slider__item">
					<div class="slider__item-background"><img class="img-fit" src="https://bi-pi.ru/upload/iblock/60f/60fc7f02c0f51c636eccd4542817014f.jpg"></div>
					<div class="inner">
						<div class="slider__item-content">
							<div class="slider__item-title">Юридическое сопровождение и IT- поддержка <br> от 3000 руб.</div>
							<div class="slider__item-btn"><a class="btn btn_red" href="#"><span>Узнать подробнее</span></a></div>
						</div>
					</div>
				</div>
				<div class="slider__item">
					<div class="slider__item-background"><img class="img-fit" src="https://bi-pi.ru/upload/iblock/3c9/3c953f00b9803e8623488f8f087849a4.jpg"></div>
					<div class="inner">
						<div class="slider__item-content">
							<div class="slider__item-title">Переход на онлайн- кассы. Кому это нужно?</div>
							<div class="slider__item-btn"><a class="btn btn_red" href="#"><span>Узнать подробнее</span></a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="slider__arrow slider__arrow_prev">
			<div class="fa fa-chevron-left"></div>
		</div>
		<div class="slider__arrow slider__arrow_next">
			<div class="fa fa-chevron-right"></div>
		</div>
		<div class="slider__dots"></div>
	</div>
	<div class="features">
		<div class="inner">
			<div class="features__wrap">
				<div class="features__list">
					<div class="features__item">
						<div class="features__item-icon"><img src="<?=base_url()?>assets/img/features/main-icon-1.svg"></div>
						<div class="features__item-text">Постоянный контроль изменений в законодательстве, с целью оптимизации бизнес-процессов.</div>
					</div>
					<div class="features__item">
						<div class="features__item-icon"><img src="<?=base_url()?>assets/img/features/main-icon-2.svg"></div>
						<div class="features__item-text">Передача на аутсорсинг любого участка учета</div>
					</div>
					<div class="features__item">
						<div class="features__item-icon"><img src="<?=base_url()?>assets/img/features/main-icon-3.svg"></div>
						<div class="features__item-text">Трансформация финансовой отчетности по стандартам МСФО, с целью привлечения зарубежных инвестиций</div>
					</div>
					<div class="features__item">
						<div class="features__item-icon"><img src="<?=base_url()?>assets/img/features/main-icon-4.svg"></div>
						<div class="features__item-text">Организация бизнес-процессов с нуля для начинающих предпринимателей</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="announce">
		<div class="inner">
			<div class="announce__title">
				<h1>Аутсорсинг бухгалтерских услуг</h1>
			</div>
			<div class="announce__list">
				<div class="announce__item">
					<div class="announce__item-inner">
						<div class="announce__item-image"><a href="#"><img src="https://bi-pi.ru/upload/iblock/e98/e98629710e6c782c5885da44d7151298.jpg"></a></div>
						<div class="announce__item-title"><a href="#">Бухгалтерский, налоговый, кадровый учет и отчетность</a></div>
						<div class="announce__item-text">Комплекс услуг по организации и ведению любого учета для предприятий и индивидуальных предпринимателей с любой формой собственности, режимами налогообложения и видами деятельности.</div>
						<div class="announce__item-btn"><a class="btn btn_red" href="#"><span>Подробнее</span></a></div>
					</div>
				</div>
				<div class="announce__item">
					<div class="announce__item-inner">
						<div class="announce__item-image"><a href="#"><img src="https://bi-pi.ru/upload/iblock/870/870bdce5816968b56efcb20caf452a16.jpg"></a></div>
						<div class="announce__item-title"><a href="#">Управленческий, финансовый учет и анализ, стандарты МСФО</a></div>
						<div class="announce__item-text">Отчетность по МСФО представляет большую ценность для инвесторов, поскольку отражает реальное положение дел, наиболее удобна для анализа и принятия важных управленческих решений.</div>
						<div class="announce__item-btn"><a class="btn btn_red" href="#"><span>Подробнее</span></a></div>
					</div>
				</div>
				<div class="announce__item">
					<div class="announce__item-inner">
						<div class="announce__item-image"><a href="#"><img src="https://bi-pi.ru/upload/iblock/5d0/5d0921da1b558d9e93a0ccf66d258f6b.jpg"></a></div>
						<div class="announce__item-title"><a href="#">Управление Бизнесом. Юридическое сопровождение и IT- поддержка</a></div>
						<div class="announce__item-text">Не секрет, что управление бизнесом – это, прежде всего профессионализм. Чтобы эффективно управлять бизнесом, необходимо просчитать ситуации, при которых понадобятся профессиональные исполнители.</div>
						<div class="announce__item-btn"><a class="btn btn_red" href="#"><span>Подробнее</span></a></div>
					</div>
				</div>
			</div>
			<div class="announce__more"><a class="btn btn_red" href="#"><span>Все услуги</span></a></div>
		</div>
	</div>
	<div class="announce">
		<div class="inner">
			<div class="announce__title">
				<h2>Кому будут полезны наши услуги</h2>
			</div>
			<div class="announce__list">
				<div class="announce__item">
					<div class="announce__item-inner">
						<div class="announce__item-image"><img src="https://bi-pi.ru/upload/iblock/098/098c5c15510540be0f2ab0acfdb9d9b5.jpg"></div>
						<div class="announce__item-title">ИП и микро-бизнесу</div>
						<div class="announce__item-text">Необходима помощь бухгалтера, чтобы освободить время для расширения и развития бизнеса.</div>
					</div>
				</div>
				<div class="announce__item">
					<div class="announce__item-inner">
						<div class="announce__item-image"><img src="https://bi-pi.ru/upload/iblock/297/297d3c94f8397fcb8df5016eb2e9047e.jpg"></div>
						<div class="announce__item-title">Организациям малого и среднего бизнеса</div>
						<div class="announce__item-text">Вы можете быть компанией со штатом от 10 до 250 сотрудников, и вам периодически нужна помощь и консультации профессиональных финансистов в области учета и налогообложения.</div>
					</div>
				</div>
				<div class="announce__item">
					<div class="announce__item-inner">
						<div class="announce__item-image"><img src="https://bi-pi.ru/upload/iblock/edb/edb328630d16066e22809cbd902aa7d0.jpg"></div>
						<div class="announce__item-title">Предприятиям крупного бизнеса</div>
						<div class="announce__item-text">Передают на аутсорсинг нашей компании отдельные функции по ведению бухгалтерского учета.</div>
					</div>
				</div>
			</div>
			<div class="announce__more"><a class="btn btn_red" href="#"><span>Все услуги</span></a></div>
		</div>
	</div>
	<div class="announce">
		<div class="inner">
			<div class="announce__title">
				<h2>О нас</h2>
			</div>
			<div class="announce__text">Группа компаний БиПИ под руководством аттестованного профессионального аудитора предлагает ведение любого учета Вашей организации, а также участие в проведении аудиторских проверок финансово-хозяйственной деятельности организаций любой формы собственности. Кроме этого, мы предлагаем комплексные услуги автоматизации учетных процессов, <a href="#">бухгалтерский аутсорсинг</a>, юридическое сопровождение, <a href="#">помощь в отчетности для ИП</a> и экспресс-консультации.</div>
		</div>
	</div>
	<div class="announce">
		<div class="inner">
			<div class="announce__title">
				<h2>Наши клиенты</h2>
			</div>
			<div class="announce__carousel">
				<div class="announce__carousel-wrap">
					<div class="announce__carousel-list">
						<div class="announce__carousel-item"><img src="https://bi-pi.ru/upload/resize_cache/iblock/d69/200_150_2/d699c6c25ef4e6788ddcc9087c8715a4.png"></div>
						<div class="announce__carousel-item"><img src="https://bi-pi.ru/upload/resize_cache/iblock/39a/200_150_2/39a53fd7640801bc3c6b87479a04dacc.png"></div>
						<div class="announce__carousel-item"><img src="https://bi-pi.ru/upload/iblock/dec/decfaa2e074b80cf64d97afbd6f64b02.png"></div>
						<div class="announce__carousel-item"><img src="https://bi-pi.ru/upload/iblock/f54/f5419ec2bb4f616fcfd7d74dcb1d893d.png"></div>
						<div class="announce__carousel-item"><img src="https://bi-pi.ru/upload/iblock/ee5/ee5cfb843936633df11afba290fd80d4.png"></div>
						<div class="announce__carousel-item"><img src="https://bi-pi.ru/upload/iblock/42e/42eb8c5049f1bce1a943016b147d35d1.png"></div>
						<div class="announce__carousel-item"><img src="https://bi-pi.ru/upload/iblock/29c/29cb7d1b1064a10e8007ff2e1bf4d3b7.png"></div>
						<div class="announce__carousel-item"><img src="https://bi-pi.ru/upload/iblock/a5c/a5c463fd4bb9545d124a038c77240cad.png"></div>
						<div class="announce__carousel-item"><img src="https://bi-pi.ru/upload/iblock/a9e/a9e2331ce1710c7692d152d46a8c5302.png"></div>
					</div>
				</div>
				<div class="announce__carousel-arrow announce__carousel-arrow_prev">
					<div class="fa fa-chevron-left"></div>
				</div>
				<div class="announce__carousel-arrow announce__carousel-arrow_next">
					<div class="fa fa-chevron-right"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="inner">
		<div class="consult">
			<div class="consult__wrap">
				<div class="consult__content">
					<div class="consult__title">Нужна консультация специалиста?</div>
					<div class="consult__text">
						<p>Звоните 8 (800) 250-86-47, 8 (812) 600-51-78</p>
						<p>Или оставьте заявку и мы перезвоним Вам в ближайшее время!</p>
					</div>
					<div class="consult__form">
						<form class="form form_ajax" action="<?=base_url()?>forms/order_call" method="post" novalidate autocomplete="off">
							<div class="form__wrap">
								<div class="form__field form__field_2">
									<div class="form__input">
										<input type="text" name="fio" data-require="true" placeholder="Ваше имя">
									</div>
								</div>
								<div class="form__field form__field_2">
									<div class="form__input">
										<input type="text" name="phone" data-require="true" data-mask="+7 (999) 999-99-99" placeholder="Ваш номер телефона">
									</div>
								</div>
								<div class="form__field">
									<div class="form__input">
										<label>
											<input type="checkbox" name="confirm" data-require="true">
											<div class="form__label">Соглашаюсь на <a href="#" target="_blank">обработку персональных данных</a></div>
										</label>
									</div>
								</div>
								<div class="form__field form__field_submit">
									<div class="form__input">
										<button class="btn btn_red btn_fill" type="submit"><span>Отправить заявку</span></button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="consult__image"><img src="https://bi-pi.ru/upload/medialibrary/b36/b368976866726c907c6ba6ea308c8ab4.jpg"></div>
			</div>
		</div>
	</div>
</div>