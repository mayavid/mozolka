<div class="website">
	<div class="article">
		<div class="inner">
			<div class="article__wrap">
				<div class="article__content">
					<div class="breadcrumbs"><a class="breadcrumbs__item" href="<?=base_url()?>">Главная</a><a class="breadcrumbs__item" href="<?=base_url()?>news">Новости</a>
						<div class="breadcrumbs__item"><?=$item['anchor']?></div>
					</div>
					<div class="content">
						<p><img class="lazy" data-src="<?=$item['image']?>"></p>
						<p><?=$item['added']?></p>
						<?=$item['html']?>
					</div>
					<div class="go-back"><a href="<?=base_url()?>news">Назад в новости</a></div>
					<div class="share">
						<!--share-->
						<div class="block_friends">
							<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
							<script src="//yastatic.net/share2/share.js" charset="utf-8"></script>
							<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter" data-counter=""></div>
						</div>
						<!--/share-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>