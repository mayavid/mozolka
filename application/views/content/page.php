<div class="website">
	<div class="article">
		<div class="inner">
			<div class="article__wrap">
				<div class="article__aside">
					<?if(count($pages) != 0) {?>
						<div class="article__aside-menu">
							<div class="article__menu">
								<div class="article__menu-list">
									<?foreach ($pages as $item):?>
										<a class="article__menu-item <?=($item['slug'] == $this->uri->segment($this->uri->total_segments())) ? "is-active" : null;?>" href="<?=$item['link']?>"><?=$item['title']?></a>
									<?endforeach;?>
								</div>
							</div>
						</div>
					<?}?>
					<div class="article__aside-stick">
						<div class="article__stick">
							<div class="downloadprice"><a class="downloadprice__inner" href="<?=base_url()?>images/price_list.pdf" target="_blank">
									<div class="downloadprice-icon"><i class="fa fa-file-pdf-o"></i></div>
									<div class="downloadprice-text">Скачать прайслист</div></a></div>
						</div>
					</div>
				</div>
				<div class="article__content">
					<div class="breadcrumbs">
						<?foreach ($breadcrumbs as $item):?>
							<?=$item;?>
						<?endforeach;?>
					</div>
					<div class="content">
						<?=$html;?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>