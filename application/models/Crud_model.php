<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Crud_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->clean_sessions();
    }

    //Чистка сессий (на всякий случай, чтобы не плодились)
    public function clean_sessions()
    {
        $this->db->where('timestamp <', (time() - $this->config->item('sess_expiration')));
        $this->db->delete('ci_sessions');
    }

    //Основные выборки
    public function get_status_db()
    {
        $query = $this->db->query("SHOW TABLE STATUS");
        return $query->result_array();
    }

    public function get_all($table)
    {
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_all_sort($table, $column, $sort)
    {
        $this->db->order_by($column, $sort);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_limit_sort($limit, $table, $column, $sort)
    {
        $this->db->limit($limit);
        $this->db->order_by($column, $sort);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_all_sort_pagination($table, $column, $sort, $start_from, $limit)
    {
        $this->db->limit($limit, $start_from);
        $this->db->order_by($column, $sort);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_by_id($table, $field_id, $id)
    {
        $this->db->where($field_id, $id);
        $query = $this->db->get($table);
        return $query->row_array();
    }

    public function get_all_by_id($table, $field_id, $id)
    {
        $this->db->where($field_id, $id);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_all_by_id_sort($table, $field_id, $id, $column, $sort)
    {
        $this->db->order_by($column, $sort);
        $this->db->where($field_id, $id);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_all_like_id_sort($table, $field_id, $id, $column, $sort)
    {
        $this->db->order_by($column, $sort);
        $this->db->like($field_id, $id);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_all_by_id_sort_pagination($table, $field_id, $id, $column, $sort, $start_from, $limit)
    {
        $this->db->limit($limit, $start_from);
        $this->db->order_by($column, $sort);
        $this->db->where($field_id, $id);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_all_by_array($table, $field_id, $array)
    {
        $this->db->where_in($field_id, $array);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_all_by_array_sort($table, $field_id, $array, $column, $sort)
    {
        $this->db->order_by($column, $sort);
        $this->db->where_in($field_id, $array);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function update_by_id($table, $field_id, $id, $upddata)
    {
        $this->db->where($field_id, $id);
        $this->db->update($table, $upddata);
    }

    public function insert_data($table, $insdata)
    {
        $this->db->insert($table, $insdata);
        return $this->db->insert_id();
    }

    public function count_all($table)
    {
        return $this->db->count_all($table);
    }

    public function count_by_id($table, $field_id, $id)
    {
        $this->db->where($field_id, $id);
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    public function count_by_id_null($table, $field_id)
    {
        $this->db->where($field_id . " IS NULL");
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    public function count_by_id_notnull($table, $field_id)
    {
        $this->db->where($field_id . " IS NOT NULL");
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    public function count_like_id($table, $field_id, $id)
    {
        $this->db->like($field_id, $id);
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    public function count_all_by_array($table, $field_id, $array)
    {
        $this->db->where_in($field_id, $array);
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    public function delete_by_id($table, $field_id, $id)
    {
        $this->db->where($field_id, $id);
        $this->db->delete($table);
    }

    public function delete_by_array($table, $field_id, $array)
    {
        $this->db->where_in($field_id, $array);
        $this->db->delete($table);
    }

    //Конец основных выборок
}