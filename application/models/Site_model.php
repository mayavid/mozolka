<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Site_model extends Crud_model
{
    public function __construct()
    {
        parent::__construct();
    }

	public function get_similar_sections($parent)
	{
		$this->db->where('parent', $parent);
		$query = $this->db->get('sections');
		return $query->result_array();
	}

	public function get_sections_for_bottom()
	{
		$this->db->order_by('rating', 'DESC');
		$this->db->where('parent', 0);
		$this->db->where('publish', 1);
		$this->db->where('id_page IS NOT NULL');
		$query = $this->db->get('sections');
		return $query->result_array();
	}

	public function get_last_news($count)
	{
		$this->db->limit($count);
		$this->db->order_by('added', 'DESC');
		$this->db->where('publish', 1);
		$query = $this->db->get('news');
		return $query->result_array();
	}

	public function count_news()
	{
		$this->db->where('publish', 1);
		$this->db->from('news');
		return $this->db->count_all_results();
	}

	public function get_news($limit, $start_from)
	{
		$this->db->limit($limit, $start_from);
		$this->db->order_by('added', 'DESC');
		$this->db->where('publish', 1);
		$query = $this->db->get('news');
		return $query->result_array();
	}

	public function get_articles($id_articles_section, $limit, $start_from)
	{
		$this->db->limit($limit, $start_from);
		$this->db->order_by('added', 'DESC');
		$this->db->where('publish', 1);
		$this->db->where('id_articles_section IS NOT NULL');
		if($id_articles_section != null)
			$this->db->where('id_articles_section', $id_articles_section);
		$query = $this->db->get('articles');
		return $query->result_array();
	}

	public function count_articles($id_articles_section)
	{
		$this->db->where('publish', 1);
		$this->db->where('id_articles_section IS NOT NULL');
		if($id_articles_section != null)
			$this->db->where('id_articles_section', $id_articles_section);
		$this->db->from('articles');
		return $this->db->count_all_results();
	}

	public function search_news($str)
	{
		$this->db->order_by('added', 'DESC');
		$this->db->like('title', $str);
		$this->db->where('publish', 1);
		$query = $this->db->get('news');
		return $query->result_array();
	}

	public function search_articles($str)
	{
		$this->db->order_by('added', 'DESC');
		$this->db->like('title', $str);
		$this->db->where('publish', 1);
		$this->db->where('id_articles_section IS NOT NULL');
		$query = $this->db->get('articles');
		return $query->result_array();
	}

	public function search_pages($str)
	{
		$this->db->order_by('id', 'DESC');
		$this->db->like('title', $str);
		$this->db->where('publish', 1);
		$query = $this->db->get('pages');
		return $query->result_array();
	}
}