<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends Crud_model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_admin_by_token($token)
    {
        $this->db->where('token', $token);
        $query = $this->db->get('admin_users');
        return $query->row_array();
    }

    public function get_admin_by_credentials($login, $password)
    {
        $this->db->where('login', $login);
        $this->db->where('password', $password);
        $query = $this->db->get('admin_users');
        return $query->row_array();
    }

	public function get_short_page($id)
	{
		$this->db->select('id, title, publish');
		$this->db->where('id', $id);
		$query = $this->db->get('pages');
		return $query->row_array();
	}

	public function get_short_pages()
	{
		$this->db->select('id, title, publish');
		$query = $this->db->get('pages');
		return $query->result_array();
	}

	public function get_short_def_page($id)
	{
		$this->db->select('id, title');
		$this->db->where('id', $id);
		$query = $this->db->get('def_pages');
		return $query->row_array();
	}

	public function get_short_def_pages()
	{
		$this->db->select('id, title');
		$query = $this->db->get('def_pages');
		return $query->result_array();
	}

}