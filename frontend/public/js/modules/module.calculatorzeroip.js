import watcher from './module.watcher'
import Dep  from './module.depclass'

let calcZeroIp = document.querySelector('#calcZeroIp');

if(calcZeroIp){
	
	let priceItemWrapIp = document.querySelectorAll(".calc #calcZeroIp .calc__price");
	let formDataCalcZero = new FormData(calcZeroIp);

	let baseDataIp = {};
	baseDataIp.formnalog = formDataCalcZero.get('formnalog');
	baseDataIp.period = formDataCalcZero.get('period');
	baseDataIp.delivery = formDataCalcZero.get('delivery');

	let dataPriceIp = {
		"usn quarter"		: 1500 ,
		"usn year"   		: 2000,
		"osno quarter"     	: 2000,
		"osno year"        	: 2500 ,
		"osno envd quarter"	: 2500 ,
		"osno envd year"    : 3000,
		"usn envd quarter"  : 2500 ,
		"usn envd year"     : 3000,
		"dntsend"    		: 0,
		"postoffice"  		: 900,
		"edo"  	      		: 350,
		"narochnym"   		: 1100,
	};



	
	Object.keys(baseDataIp).forEach(key => {
	    let internalValue = baseDataIp[key]

	    const dep = new Dep()

	    Object.defineProperty(baseDataIp, key, {
	        get() {
	            dep.depend() // запоминаем выполняемую функцию target
	            return internalValue
	        },
	        set(newVal) {
	            internalValue = newVal
	            dep.notify() // повторно выполняем сохранённые функции
	        }
	    })
	});



	watcher(() => {
		let priceFormNalogPeriod = `${baseDataIp.formnalog} ${baseDataIp.period}`;
		let priceDelivery = `${baseDataIp.delivery}`;

		baseDataIp.total = dataPriceIp[priceFormNalogPeriod] + dataPriceIp[priceDelivery];
	    
	    priceItemWrapIp[1].innerText = `${ dataPriceIp[priceFormNalogPeriod] } руб.`;
	    priceItemWrapIp[2].innerText = `${ dataPriceIp[priceDelivery] } руб.`;
	    priceItemWrapIp[3].innerText = `${baseDataIp.total} руб.`; 

	});


	document.addEventListener('change', (e) => {
			e.preventDefault();

			let formDataCalcZero = new FormData(calcZeroIp);

			baseDataIp.formnalog = formDataCalcZero.get('formnalog');
			baseDataIp.period = formDataCalcZero.get('period');
			baseDataIp.delivery = formDataCalcZero.get('delivery');			
		});
}
