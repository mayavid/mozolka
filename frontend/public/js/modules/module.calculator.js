import watcher from './module.watcher'
import Dep  from './module.depclass'

let calc = document.querySelector('#calc');

if (calc) {

	let priceItemWrap = document.querySelectorAll(".calc #calc .calc__price");
	let formDataCalc = new FormData(calc);

	let calcData = {};

		calcData.formnalog = formDataCalc.get('formnalog');
		calcData.kindactivity = formDataCalc.get('activity');
		calcData.operations = formDataCalc.get('operations');
		calcData.staff = formDataCalc.get('staff');	
		

	let setPrice = {
		"usn gain services" : 6000,
		"usn gain trade" : 8000,
		"usn gain production" : 10000,
		"usn gain expenses services"  : 10000,
		"usn gain expenses trade"  : 12000,
		"usn gain expenses production"  : 15000,
		"envd services" : 6000,
		"envd trade" : 8000,
		"envd production" : 10000,
		"combination usn envd services"  : 15000,
		"combination usn envd trade" : 18000,
		"combination usn envd production" : 25000,
		"osno services" : 15000,
		"osno trade" : 18000,
		"osno production" : 25000,
		"combination osno envd services"  : 20000,
		"combination osno envd trade" : 25000,
		"combination osno envd production" : 30000,
		"before fifty services": 0,
		"before fifty trade": 0,
		"before fifty production": 0,
		"after fifty services" : 0.05,
		"after fifty trade" : 0.1,
		"after fifty production" : 0.15,
		"after hundred services" : 0.1,
		"after hundred trade" : 0.15,
		"after hundred production" : 0.2,
		"staff before five": 0,
		"staff after five": 0.1,
		"staff after ten": 0.4,
	};



	
	Object.keys(calcData).forEach(key => {
	    let internalValue = calcData[key]

	    const dep = new Dep()

	    Object.defineProperty(calcData, key, {
	        get() {
	            dep.depend() 
	            return internalValue
	        },
	        set(newVal) {
	            internalValue = newVal
	            dep.notify() 
	        }
	    })
	});


	watcher(() => {
		
		let priceKindNalogActivity = `${calcData.formnalog} ${calcData.kindactivity}`;
		let priceOperations = `${calcData.operations} ${calcData.kindactivity}`;
		let priceStaff = `${calcData.staff}`;


		let basePrice = setPrice[priceKindNalogActivity];
		let markupOperations = basePrice * setPrice[priceOperations];
		let markupStaff = setPrice[priceKindNalogActivity] * setPrice[priceStaff];

		calcData.total = basePrice + markupOperations + markupStaff;

	    priceItemWrap[0].innerHTML = `${calcData.total} руб.`; 

	});


	document.addEventListener('change', (e) => {
			e.preventDefault();

			let formDataCalc = new FormData(calc);
			calcData.formnalog = formDataCalc.get('formnalog');
			calcData.kindactivity = formDataCalc.get('activity');
			calcData.operations = formDataCalc.get('operations');
			calcData.staff = formDataCalc.get('staff');			
		});
};