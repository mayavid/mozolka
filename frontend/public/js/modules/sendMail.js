'use strict';
import axios from 'axios';
import UIkit from 'uikit';

class sendOrder{
	constructor(formOrderObject){

		this.button = formOrderObject.buttonForm;
		this.form = formOrderObject.form;
		this.resolutionSendForm = false;
		


		// Суть проверки поля по регулярным выражениям:
		// Ругулярные выражения ищут все символы, которы НЕ ДОЛЖНЫ БЫТЬ В ПОЛЕ
		// Далее с помощью функции split разбиваем данные введенные в поле 
		// по разделителям (разделителями являются не нужные данные, те которые удовлетворяют рег выраж) удовлетворяющим регулярному выражению на элементы массива.
		// Потом с помощью функции join заново собираем фразу, но у же без НЕНУЖНЫХ символов и вставляем их в поле input


		this.RegRulForFields = {
			phone : /^\+7\(\d{3}\) \d{3}-\d{2}-\d{2}$/im,
			name  : /[^А-ЯЁа-яёA-Za-z\s]{1,200}/img,
			mail  : /^.+@.+\..+$/im,
			text  : /[^А-ЯЁа-яё\d\s\.\,]{1,200}/img,
		};



		this.noticeField = {
			phone : '<div class="uk-padding uk-box-shadow-large uk-text-center" style="background: #FFF176; color: #006699">Проверьте правильность заполнения поля:<p> "Номер Вашего телефона"</p></div>',
			name  : '<div class="uk-padding uk-box-shadow-large uk-text-center" style="background: #FFF176; color: #006699">Заполните пожалуйста поле:<p> "Имя Отчетство"</p><p class="uk-text-small">Допустимы только русские и английские буквы</p></div>',
			mail  : 'Проверьте правильность заполнения поля mail',
			text  : 'Не заполнено поле text'	
		};

		this.fieldNameForm = this.getFieldNameForm();
		this.itemsFormObj = this.getItemForm();	


		this.form.addEventListener('change', (e) => {
			e.preventDefault();


			let indicatorFilled = this.verifyFilledForm(e);


			this.resolutionSendForm = indicatorFilled.totalField;

			if(indicatorFilled.totalField){
				this.button.removeAttribute('disabled');

			} else {
				this.button.setAttribute('disabled', '');
			}

		});


		this.button.addEventListener('click', (e) => {
			e.preventDefault();
			let formIs = this.form;
			let feedBackFormData = new FormData(formIs); 
			

			if( !this.button.hasAttribute('disabled') && this.resolutionSendForm ){
				console.log(document.location.href);
				axios({
						url: document.location.href,
						method: 'post',
						transformRequest: [function(){
							let feedBackFormData = new FormData(formIs); 
							return feedBackFormData;
						}],
						headers: { 
									'X-Requested-With': 'XMLHttpRequest',
									'Content-Type' : 'multipart/form-data'
						},

					}).
					then( (response) => {

						console.log( response.data);
										
					}).
					catch({});
			}
			

		});
	};

	getFieldNameForm(){

		let frmDataOrder = new FormData(this.form);
		let nameFieldFormArr = [];

		for( let coupleData of frmDataOrder.entries() ){			
			
			nameFieldFormArr.push(coupleData[0]);
		};
		
		return nameFieldFormArr;
	};

	getItemForm(){
		let itemForm = {};

		this.fieldNameForm.forEach( (currentValue, i) => {
			
			itemForm[currentValue] = this.form[currentValue];
		});

		return itemForm;		
	};

	verifyFilledForm(e){
		let frmDataOrder = new FormData(this.form);
		let indicatorFilledFormObj = {};
		let indicatorFilledForm = true;

		for( let coupleData of frmDataOrder.entries() ){
			
			let itemFormFieldName = coupleData[0];

			let itemFieldNameForRegRul = coupleData[0].split('-')[0].trim();

			let itemFormFieldValue = coupleData[1].trim();
			let RegItem =  this.RegRulForFields[itemFieldNameForRegRul];

			switch (itemFieldNameForRegRul){
				case 'phone': 
						indicatorFilledFormObj[itemFormFieldName] = RegItem.test(itemFormFieldValue) && true;

						if(indicatorFilledFormObj[itemFormFieldName]){
							this.form[itemFormFieldName].classList.remove('uk-form-danger');							
						} else {
							this.form[itemFormFieldName].classList.add('uk-form-danger');

							if(e.target.name == itemFormFieldName){
								UIkit.notification({
								    message: this.noticeField[itemFieldNameForRegRul],
								    status: 'primary',
								    pos: 'top-center',
								    timeout: 3000
								});								
							};
						};
						break;

				case 'name':
						indicatorFilledFormObj[itemFormFieldName] = itemFormFieldValue && true;
						
						if(indicatorFilledFormObj[itemFormFieldName]){
							this.form[itemFormFieldName].classList.remove('uk-form-danger');
							this.form[itemFormFieldName].value = itemFormFieldValue.split(this.RegRulForFields[itemFieldNameForRegRul]).join('');

							if(this.form[itemFormFieldName].value == ""){

								UIkit.notification({
								    message: this.noticeField[itemFieldNameForRegRul],
								    status: 'primary',
								    pos: 'top-center',
								    timeout: 5000
								});
							}

						} else {
							this.form[itemFormFieldName].classList.add('uk-form-danger');

							if(e.target.name == itemFormFieldName){

								UIkit.notification({
								    message: this.noticeField[itemFieldNameForRegRul],
								    status: 'primary',
								    pos: 'top-center',
								    timeout: 5000
								});
							}
						}
						break;

				case 'mail':
						indicatorFilledFormObj[itemFormFieldName] = RegItem.test(itemFormFieldValue) && true;

						if(indicatorFilledFormObj[itemFormFieldName]){
							this.form[itemFormFieldName].classList.remove('uk-form-danger');							
						} else {
							this.form[itemFormFieldName].classList.add('uk-form-danger');
						};
						break;

				case 'text': 
						indicatorFilledFormObj[itemFormFieldName] = itemFormFieldValue && true;

						if(indicatorFilledFormObj[itemFormFieldName]){
							this.form[itemFormFieldName].classList.remove('uk-form-danger');
							this.form[itemFormFieldName].value = itemFormFieldValue.split(this.RegRulForFields[itemFieldNameForRegRul]).join('');

						} else {
							this.form[itemFormFieldName].classList.add('uk-form-danger');
						};
						break;
			}


			indicatorFilledFormObj.totalField = indicatorFilledFormObj[itemFormFieldName] && indicatorFilledForm;
			

		}

		return indicatorFilledFormObj;
	};



};





export default sendOrder;