import dataFunc from './module.target'

function watcher(myFunc){
    dataFunc.target = myFunc;
    dataFunc.target();
    dataFunc.target = null;
};


export default watcher