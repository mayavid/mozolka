import $ from 'jquery'
import Forms from './module.validate'
import Inputmask from 'inputmask'

import './datepicker';
import 'select2/dist/js/select2.js';

window.app = window.app || {};

$(()=>{

	/*let data = [
		{
			name: 'password',
			error: true,
			success: true,
			message: 'фвфывфы'
		}
	];*/

	let scrollTimer = null;

	// Вывод ошибки
	class Form extends Forms {
		result (data, $form){
			for (let i = 0; i < data.length; i ++){

				let $input = $form.find(`[name = "${data[i].name}"]`),
					$field = $input.closest('.form__field'),
					$message = $field.find('.form__message')
				;

				if (!$message.length){
					$message = $('<div class="form__message"></div>').appendTo($field.eq($field.length - 1));
				}

				if (data[i].error) $field.addClass('f-error');
				if (data[i].success) $field.addClass('f-success');
				if (data[i].message) {
					$field.addClass('f-message');
					$message.html(data[i].message)
				}
			}

			/*if ($('.f-error').length) {

				clearTimeout(scrollTimer);
				scrollTimer = setTimeout(()=>{
					let offset = window.app.breakpoint === 'mobile' ? 140 : 20;
					$('html,body').animate({scrollTop: $('.f-error').eq(0).offset().top - offset}, 500);

				}, 200);

			}*/
		}

		checkFilled(input){
			let $input = $(input),
				$field = $input.closest('.form__field');
			$field.removeClass('f-focused');

			if ( !$.trim($input.val()) ){
				$field.removeClass('f-filled');
			} else {
				$field.addClass('f-filled');
			}
		}
	}

	const form = new Form();

	$('input[data-mask]').each(function () {
		let $t = $(this),
			inputmask = new Inputmask({
				mask: $t.attr('data-mask'),		//'+7 (999) 999-99-99',
				showMaskOnHover: false,
				onincomplete: function() {
					this.value = '';
					$t.closest('.form__field').removeClass('f-filled');
				}
			});
		inputmask.mask($t[0]);
	});

	$('.form__field input, .form__field select').each(function(e){
		if (this.name) form.checkFilled(this);
	});



	window.app.formValidate = function(el, error){
		let $form = $(el),
			$item = $form.find('input, textarea, select').filter(function(){return $(this).closest('.form__input').is(':visible')}),
				wrong = false
			;

			$item.each(function () {
				let input = $(this), rule = $(this).attr('data-require');

				if ( !input.is(':visible') ) return false;

				$(input)
					.closest('.form__field')
					.removeClass('f-error f-message f-success')
					.find('.form__message')
					.html('')
				;

				if ( rule ){
					form.validate(input[0], rule, err =>{
						if (err.errors.length) {
							wrong = true;
							let data = [{
								name: input[0].name,
								error: true,
								message: err.errors[0]
							}];

							form.result(data, $form);

						}
					})
				}
			});
			if ( wrong ){
			if ( error ) error();
		}
	};


	$('.form__field select').select2({
		width: '100%'
	});

	$('.form__field input[type=file]').change(function (e) {
		let $t = $(this),
			$field = $t.closest('.form__field')
		;

		if ( e.target.files.length ){
			$field.find('.form__label').text(`${e.target.files.length.declension('Выбран', 'Выбрано', 'Выбрано')} ${e.target.files.length} ${e.target.files.length.declension('файл', 'файла', 'файлов')} `);
		}

	});

	$('.form__field input[data-date]').each(function () {
		let $t = $(this),
			$form = $t.closest('form'),
			type = $t.attr('data-date'),
			range = type.indexOf('range') === 0,
			$point,
			dp
		;

		dp = $t.datepicker({
			maxDate: new Date(),
			offset: 0,
			range,
			autoClose: true,
			multipleDatesSeparator: ' - ',
			prevHtml: `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 7 12"><polygon points="4.8,11.5 0.6,6 4.8,0.5 6.4,1.7 3.1,6 6.4,10.3 "/></svg>`,
			nextHtml: `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 7 12"><polygon points="2.2,0.5 6.4,6 2.2,11.5 0.6,10.3 3.9,6 0.6,1.7 "/></svg>`,
			//titleCTA: `за последнюю неделю`,
			navTitles: {
				days: `MM yyyy`
			},
			onSelect: (formattedDate, date, inst)=>{
				if ( inst.selectedDates.length > 1 ) inst.$el.data('datepicker').hide()
			},
			onShow: (e)=>{
				//e.$el.closest('.filter__item').addClass('is-open');


				switch (type){
					case 'from':
						$point = $form.find('[data-date=to]');
						if ($point.length && $point.val()) {
							dp.data('datepicker').update({
								maxDate: new Date($point.val().replace( /(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1"))
							})
						}
						break;
					case 'to':
						$point = $form.find('[data-date=from]');
						if ($point.length && $point.val()) {
							dp.data('datepicker').update({
								minDate: new Date($point.val().replace( /(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1"))
							})
						}
						break;
					case 'range-from':
						$point = $form.find('[data-date=range-to]');
						if ($point.length && $point.val()) {
							let val = $point.val().split(' - ')[0];
							dp.data('datepicker').update({
								maxDate: new Date(val.replace( /(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1"))
							})
						}
						break;
					case 'range-to':
						$point = $form.find('[data-date=range-from]');
						if ($point.length && $point.val()) {
							let val = $point.val().split(' - ');
							val = val[1] || val[0];
							dp.data('datepicker').update({
								minDate: new Date(val.replace( /(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1"))
							})
						}
						break;
					default:
						break;
				}

			},
			onHide: (e)=>{
				//e.$el.closest('.filter__item').removeClass('is-open');
			}
		});


		/*if ( $t.val() ){
			let range = $t.val().split(' - ');


			try {
				dp.data('datepicker').selectDate(range.map((d)=> new Date(d.replace( /(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1")) ));
			} catch (e) {

			}

		}*/

	});

	$('body')


		.on('blur change', '.form__field input, .form__field textarea, .form__field select', (e)=>{
			form.checkFilled(e.target)
		})

		.on('change', '[data-require]', function (e) {
			let $t = $(this),
				$input = $(`[name = "${$t[0].name}"]`),
				$field = $input.closest('.form__field');

			$field.removeClass('f-error f-message f-success')
				.find('.form__message')
				.html('')
			;

		})

		.on('submit', '.form', function (e) {
			let $form = $(this), errors = false;
			window.app.formValidate(this, (err)=>{
				e.preventDefault();
				errors = true;
			});


			if ( $form.hasClass('form_ajax') && !errors ){

				e.preventDefault();
				let $sbmt = $form.find('[type=submit]');

				window.grecaptcha.execute('6Lc4teEUAAAAAIaSeXfPxjj79ms6vhMs7WEL7gHY', {action: 'homepage'}).then((token)=>{

					if ( !$sbmt.hasClass('is-loading') ){
						$sbmt.addClass('is-loading');

						$.ajax({
							url: $form.attr('action'),
							dataType: 'json',
							type:  $form.attr('method'),
							data: `${$form.serialize()}&g-recaptcha-response=${token}`,
							//data: $form.serialize(),
							success: (res)=>{
								if (res.status){
									window.app.popup.close($form.closest('.popup'));

									if ( res.popup || res.message ){

										if ( !$('.popup_info').length ){
											window.app.popup.create('.popup_info');
										}

										window.app.popup.open('.popup_info',
											`<div class="popup__title">${res.popup.title || ''}</div>
											<div class="popup__text">${res.popup.text || res.message}</div>
											<div class="popup__btn">
											 <div class="btn btn_brown" data-popup-close=".popup"><span>Закрыть</span></div>
										</div>`);
									}

									$form[0].reset();

									if ( res.reload ) location.reload();

									if ( res.location ) location.replace(res.location);

								} else if ( res.errors && res.errors.length ){
									form.result(res.errors, $form);
								}
							},
							error: (xhr, textStatus)=>{
								let jsonAnswer = JSON.parse(xhr.responseText);
								$form.find('.form__error').html(jsonAnswer['message']);
							},
							complete: (xhr, textStatus)=>{
								$sbmt.removeClass('is-loading');
							}
						})

					}

				});
			}
		});


});