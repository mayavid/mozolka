'use strict';

export default class City{
	constructor(){
		this.mainElem = document.querySelector('header.header');
		this.phone800Elem = this.mainElem.querySelectorAll('.header__action-phones-item')[0];
		this.phoneLocalElem = this.mainElem.querySelectorAll('.header__action-phones-item')[1];
		this.cityValueElem = this.mainElem.querySelector('.header__city-value');
		this.imgCity = this.mainElem.querySelector('.header__icon img');


		this.mainElem.addEventListener('click', (e) => {
			e.preventDefault();


			if( e.target.hasAttribute('data-switch-city')){
				let nameCity = e.target.getAttribute('data-switch-city');

				localStorage.city = nameCity;
				this.setValuesCity(nameCity);

				console.log(localStorage.city);

			}

		});


		(() => {			
			let storage = localStorage.city;

			if (!storage){
				
				localStorage.city = 'spb';	
				this.setValuesCity(localStorage.city);			
				
			}else{
				
				this.setValuesCity(localStorage.city);
			}

		})();
	};	

	setValuesCity(nameCity){

		let rexPhone = /\d+/img;
		let phoneNumber = this.phoneLocalElem.getAttribute(`data-phone-${nameCity}`);
		this.phoneLocalElem.innerText = phoneNumber;
		let arrPhoneNumber = phoneNumber.match(rexPhone);
		
		let shortPhobeNumber = arrPhoneNumber.join('');
		
		this.phoneLocalElem.href=`tel:${shortPhobeNumber}`;
		this.cityValueElem.innerText = this.cityValueElem.getAttribute(`data-value-${nameCity}`);
		this.imgCity.setAttribute('src', `/img/city-${nameCity}.svg`);

	}
};








