import watcher from './module.watcher'
import Dep  from './module.depclass'

let calcZeroEntity = document.querySelector('#calcZeroEntity');

if(calcZeroEntity){

	let priceItemWrapIp = document.querySelectorAll(".calc #calcZeroEntity .calc__price");
	let formDataCalcZero = new FormData(calcZeroEntity);


	let baseDataEntity = {};
	baseDataEntity.formnalog = formDataCalcZero.get('formnalog');
	baseDataEntity.period = formDataCalcZero.get('period');
	baseDataEntity.delivery = formDataCalcZero.get('delivery');

	let dataPriceEntity = {
		"usn quarter"		: 2500 ,
		"usn year"   		: 3500,
		"osno quarter"     	: 3000,
		"osno year"        	: 5000 ,
		"osno envd quarter"	: 3500 ,
		"osno envd year"    : 5000,
		"usn envd quarter"  : 3500 ,
		"usn envd year"     : 5000,
		"dntsend"    		: 0,
		"postoffice"  		: 900,
		"edo"  	      		: 350,
		"narochnym"   		: 1100,
	};



	
	Object.keys(baseDataEntity).forEach(key => {
	    let internalValue = baseDataEntity[key]

	    
	    const dep = new Dep()

	    Object.defineProperty(baseDataEntity, key, {
	        get() {
	            dep.depend() 
	            return internalValue
	        },
	        set(newVal) {
	            internalValue = newVal
	            dep.notify() 
	        }
	    })
	});



	watcher(() => {
		let priceFormNalogPeriod = `${baseDataEntity.formnalog} ${baseDataEntity.period}`;
		let priceDelivery = `${baseDataEntity.delivery}`;

		baseDataEntity.total = dataPriceEntity[priceFormNalogPeriod] + dataPriceEntity[priceDelivery];
	    
	    priceItemWrapIp[1].innerText = `${ dataPriceEntity[priceFormNalogPeriod] } руб.`;
	    priceItemWrapIp[2].innerText = `${ dataPriceEntity[priceDelivery] } руб.`;
	    priceItemWrapIp[3].innerText = `${baseDataEntity.total} руб.`; 

	});


	document.addEventListener('change', (e) => {
			e.preventDefault();

			let formDataCalcZero = new FormData(calcZeroEntity);

			baseDataEntity.formnalog = formDataCalcZero.get('formnalog');
			baseDataEntity.period = formDataCalcZero.get('period');
			baseDataEntity.delivery = formDataCalcZero.get('delivery');			
		});
}
