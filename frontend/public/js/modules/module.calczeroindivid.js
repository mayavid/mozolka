import watcher from './module.watcher'
import Dep  from './module.depclass'

let calcZeroIndivid = document.querySelector('#indivizero');

if(calcZeroIndivid){

	let priceItemWrapIp = document.querySelectorAll(".calc #indivizero .calc__price");
	let formDataCalcZero = new FormData(calcZeroIndivid);


	let baseDataIndivid = {};
	baseDataIndivid.formnalog = formDataCalcZero.get('formnalogind');
	baseDataIndivid.period = formDataCalcZero.get('periodind');
	baseDataIndivid.delivery = formDataCalcZero.get('deliveryind');

	console.log(baseDataIndivid);

	let dataPriceEntity = {
		"patent quarter"		: 2500 ,
		"patent year"   		: 3500,
		"dntsend"    		: 0,
		"postoffice"  		: 900,
		"narochnym"   		: 1100,
	};



	
	Object.keys(baseDataIndivid).forEach(key => {
	    let internalValue = baseDataIndivid[key]

	    
	    const dep = new Dep()

	    Object.defineProperty(baseDataIndivid, key, {
	        get() {
	            dep.depend() 
	            return internalValue
	        },
	        set(newVal) {
	            internalValue = newVal
	            dep.notify() 
	        }
	    })
	});



	watcher(() => {
		let priceFormNalogPeriod = `${baseDataIndivid.formnalog} ${baseDataIndivid.period}`;
		let priceDelivery = `${baseDataIndivid.delivery}`;

		baseDataIndivid.total = dataPriceEntity[priceFormNalogPeriod] + dataPriceEntity[priceDelivery];
	    
	    priceItemWrapIp[1].innerText = `${ dataPriceEntity[priceFormNalogPeriod] } руб.`;
	    priceItemWrapIp[2].innerText = `${ dataPriceEntity[priceDelivery] } руб.`;
	    priceItemWrapIp[3].innerText = `${baseDataIndivid.total} руб.`; 

	});


	document.addEventListener('change', (e) => {
			e.preventDefault();

			let formDataCalcZero = new FormData(calcZeroIndivid);

			baseDataIndivid.formnalog = formDataCalcZero.get('formnalogind');
baseDataIndivid.period = formDataCalcZero.get('periodind');
baseDataIndivid.delivery = formDataCalcZero.get('deliveryind');
		});
}
