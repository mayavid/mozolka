import dataFunc from './module.target'

export default class Dep {
		    constructor () {
		        this.subscribers = []
		    }
		    depend () {
		        if (dataFunc.target && !this.subscribers.includes(dataFunc.target)){
		            this.subscribers.push(dataFunc.target)
		        }
		    }
		    notify () {
		        this.subscribers.forEach(sub => sub())
		    }
		};


