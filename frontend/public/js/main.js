import '../css/style.scss'

import axios from 'axios';
import Inputmask from "inputmask";

import UIkit from 'uikit'
import Icons from 'uikit/dist/js/uikit-icons'

import ImageCompare from "image-compare-viewer";


try{console.log('%cdeveloped by %c d.polyakov ovir@mail.ru ','font:bold 12px/18px monospace;color:green','font:12px/18px monospace;color:white;background-color:blue;border-radius:3px;');}catch(e){}


window.app = window.app || {};

UIkit.use(Icons);

(function(){

	const viewers = document.querySelectorAll(".image-compare");
	const formFeedBack = document.querySelector("#feedback");
	const inputPhone = formFeedBack.querySelector('[name="phone-user"]');
	const buttonFeedBackForm = formFeedBack.querySelector(".mz-button-act");	

	console.log(`Кнопка отправки формы ${buttonFeedBackForm}`);
	
	// Инициализация сравнения картинок
	viewers.forEach((element) => {
  	
	  	let view = new ImageCompare(element,{
	  		hoverStart: true,
	  		smoothingAmount: 300,
		  }).mount();

	});

	// Маска ввода телефона в форме обратной связи
	let im = new Inputmask("+7(999) 999-99-99");
	im.mask(inputPhone);

	// отправка формы


	buttonFeedBackForm.addEventListener('click', (e) =>{
		e.preventDefault();
		
		let feedBackFormData = new FormData(formFeedBack); 

		axios(
				{
					url: document.location.href,
					method: 'post',
					transformRequest: feedBackFormData,
					headers: { 
								'X-Requested-With': 'XMLHttpRequest',
								'Content-Type': 'multipart/form-data'	
					}
			})
		.then( (response) => {

				console.log( response.data);
								
			})

		.catch({});
	});



}());
