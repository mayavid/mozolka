import './css/admin.scss'
try{console.log('%cdeveloped by %c soft.maestros.ru ','font:bold 12px/18px monospace;color:green','font:12px/18px monospace;color:white;background-color:green;border-radius:3px;');}catch(e){}

import Vue from 'vue';

Vue.config.devtools = true;

import {router, store, filters, install} from './middleware'

Object.keys(filters).forEach(name => {
	Vue.filter(name, filters[name]);
});

Object.keys(install).forEach(name => {
	Vue.use(install[name].plugin, install[name].options = {});
});




import * as comps from './components';

Object.keys(comps).forEach(name => {
	Vue.component(name, comps[name]);
});

import Index from './index.vue';

if ( document.getElementById('app') ){
	new Vue({
		router,
		store,
		render: h => h(Index)
	}).$mount('#app');
}
