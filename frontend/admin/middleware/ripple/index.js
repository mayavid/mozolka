import Ripple from 'vue-ripple-directive'
Ripple.color = 'rgba(100, 100, 100, 0.25)';

export default Ripple;