import axios from './instance'

export default {

	getAll(){
		return axios.post('', {action: 'get_sections'}).then(r => r.data)
	},

	getItem(id){
		return axios.post('', {action: 'get_section', id}).then(r => r.data)
	},

	add(data){
		return axios.post('', {action: 'add_section', ...data}).then(r => r.data)
	},

	edit(data){
		return axios.post('', {action: 'edit_section', ...data}).then(r => r.data)
	},

	delete(id){
		return axios.post('', {action: 'delete_section', id}).then(r => r.data)
	},

}