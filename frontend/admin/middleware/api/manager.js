import axios from './instance'

export default {

	getAll(){
		return axios.post('', {action: 'get_admins'}).then(r => r.data)
	},

	getItem(id){
		return axios.post('', {action: 'get_admin', id}).then(r => r.data)
	},

	add(data){
		return axios.post('', {action: 'add_admin', ...data}).then(r => r.data)
	},

	edit(data){
		return axios.post('', {action: 'edit_admin', ...data}).then(r => r.data)
	},

	delete(id){
		return axios.post('', {action: 'delete_admin', id}).then(r => r.data)
	},

}