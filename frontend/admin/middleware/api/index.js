import Base from './base'
import Page from './page'
import Manager from './manager'
import Section from './section'
import News from './news'
import Article from './article'
import ArticlesSection from './articles_section'
import Part from './part'

export {
	Base,
	Page,
	Manager,
	Section,
	News,
	Article,
	ArticlesSection,
	Part,
};
