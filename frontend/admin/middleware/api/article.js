import axios from './instance'

export default {

	getAll(){
		return axios.post('', {action: 'get_articles'}).then(r => r.data)
	},

	getItem(id){
		return axios.post('', {action: 'get_article', id}).then(r => r.data)
	},

	add(data){
		return axios.post('', {action: 'add_article', ...data}).then(r => r.data)
	},

	edit(data){
		return axios.post('', {action: 'edit_article', ...data}).then(r => r.data)
	},

	delete(id){
		return axios.post('', {action: 'delete_article', id}).then(r => r.data)
	},

}