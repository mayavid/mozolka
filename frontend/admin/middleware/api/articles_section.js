import axios from './instance'

export default {

	getAll(){
		return axios.post('', {action: 'get_articles_sections'}).then(r => r.data)
	},

	getItem(id){
		return axios.post('', {action: 'get_articles_section', id}).then(r => r.data)
	},

	add(data){
		return axios.post('', {action: 'add_articles_section', ...data}).then(r => r.data)
	},

	edit(data){
		return axios.post('', {action: 'edit_articles_section', ...data}).then(r => r.data)
	},

	delete(id){
		return axios.post('', {action: 'delete_articles_section', id}).then(r => r.data)
	},

}