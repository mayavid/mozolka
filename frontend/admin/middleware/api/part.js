import axios from './instance'

export default {

	getAll(){
		return axios.post('', {action: 'get_short_def_pages'}).then(r => r.data)
	},

	getAllFull(){
		return axios.post('', {action: 'get_def_pages'}).then(r => r.data)
	},

	getItem(id){
		return axios.post('', {action: 'get_def_page', id}).then(r => r.data)
	},

	getItemShort(id){
		return axios.post('', {action: 'get_short_def_page', id}).then(r => r.data)
	},

	edit(data){
		return axios.post('', {action: 'edit_def_page', ...data}).then(r => r.data)
	},

}