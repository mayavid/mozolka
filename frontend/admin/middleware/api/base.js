import axios from './instance'

export default {

	checkAuth() {
		return axios.post('', {action: 'check_auth'}).then(r => r.data)
	},

	logIn(data) {
		return axios.post('', {action: 'login', ...data}).then(r => r.data)
	},

	logOut() {
		return axios.post('', {action: 'logout'}).then(r => r.data)
	},

	getCurrentAdmin() {
		return axios.post('', {action: 'get_current_admin'}).then(r => r.data.data)
	},


	fileUpload(data, progress = ()=>{}) {
		return axios.post('', {action: 'upload_image', ...data}, {onUploadProgress: progress}).then(r => r.data.data)
	},

	clearCache() {
		return axios.post('', {action: 'clear_cache'}).then(r => r.data)
	},

	getStatistic() {
		return axios.post('', {action: 'main'}).then(r => r.data)
	},

	searchAddress(q) {
		return axios.get('https://nominatim.openstreetmap.org/search', {
			params: {
				q,
				format:'json'
			}
		}).then(r => r.data)

	},

}