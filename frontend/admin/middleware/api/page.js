import axios from './instance'

export default {

	getAll(){
		return axios.post('', {action: 'get_short_pages'}).then(r => r.data)
	},

	getAllFull(){
		return axios.post('', {action: 'get_pages'}).then(r => r.data)
	},

	getItem(id){
		return axios.post('', {action: 'get_page', id}).then(r => r.data)
	},

	getItemShort(id){
		return axios.post('', {action: 'get_short_page', id}).then(r => r.data)
	},

	add(data){
		return axios.post('', {action: 'add_page', ...data}).then(r => r.data)
	},

	edit(data){
		return axios.post('', {action: 'edit_page', ...data}).then(r => r.data)
	},

	delete(id){
		return axios.post('', {action: 'delete_page', id}).then(r => r.data)
	},

}