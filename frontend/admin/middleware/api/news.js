import axios from './instance'

export default {

	getAll(){
		return axios.post('', {action: 'get_news'}).then(r => r.data)
	},

	getItem(id){
		return axios.post('', {action: 'get_new', id}).then(r => r.data)
	},

	add(data){
		return axios.post('', {action: 'add_new', ...data}).then(r => r.data)
	},

	edit(data){
		return axios.post('', {action: 'edit_new', ...data}).then(r => r.data)
	},

	delete(id){
		return axios.post('', {action: 'delete_new', id}).then(r => r.data)
	},

}