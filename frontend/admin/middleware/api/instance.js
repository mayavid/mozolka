import axios from 'axios'
import eventHub from "../eventhub";
import Vue from 'vue'


const STATUS_CODE = {
	1: 'Ошибка запроса',
	2: 'Неверные логин и пароль',
	3: 'Требуется авторизация',
	4: 'Нет доступа к данному разделу',
	5: 'Запись в БД не найдена',
	6: 'Запись невозможно удалить из БД',
	7: 'Изображение невозможно загрузить',
	8: 'Невозможно изменить размер изображения',
	9: 'Невозможно повернуть изображение',
	10: 'Данный slug уже используется',
	11: 'slug содержит неразрешённые символы',
};


axios.defaults.baseURL = isLocal ? 'https://bi-pi.maestros.ru/assets/test.php' : '/admin';

axios.defaults.headers.post['Content-Type'] = 'multipart/form-data';

axios.defaults.transformRequest = [(data) => {
	if ( data && data.action ){
		let formData = new FormData();

		Object.keys(data).map(function(key) {
			if (Array.isArray(data[key])) {

				for (let i = 0; i < data[key].length; i++) {
					formData.append(`${key}[]`, typeof data[key][i] === 'object' ?  JSON.stringify(data[key][i]) : data[key][i]);
				}

			} else {
				formData.append(key, data[key]);
			}
		});

		return formData;
	}
}];


let countRequest = 0;

axios.interceptors.request.use(config => {
	startLoading();
	return config;
}, error => {
	finishLoading();
	return Promise.reject(error);
});

axios.interceptors.response.use(
	response => {
		finishLoading();
		if ( response.data.status > 0 ) {
			Vue.$toast.warning(STATUS_CODE[response.data.status]);
			throw new Error(STATUS_CODE[response.data.status]);
		}
		return response;
	},
	error => {
		finishLoading();
		return Promise.reject(error);
	}
);

function startLoading() {
	if ( countRequest <= 0) eventHub.$emit('start-loading');
	countRequest++;
}

function finishLoading() {
	countRequest--;
	if ( countRequest <= 0 ) {
		countRequest = 0;
		eventHub.$emit('finish-loading');
	}
}


//
// const request = (action, ...config)=>{
//
// };


/*
const request = (action, data={}, response=()=>{}, error=()=>{}, always=()=>{}, progress=()=>{})=>{
	if (isLocal) console.info('request', data);

	data.action = action;

	return axios({
		method: 'post',
		data: data,
		onUploadProgress: (e)=>{
			console.info('onUploadProgress', e);
			progress(e);
		},
		onDownloadProgress: (e)=>{
			console.info('onDownloadProgress', e);
			progress(e);
		},
	}).then(e=>{
		if (e.data.status > 0){
			Vue.$toast.warning(STATUS_CODE[e.data.status]);
			return error(STATUS_CODE[e.data.status]);
		} else {
			return response(e.data);
		}
	}).catch(e=>{
		console.error('load error', e);
		return error(e);
	}).finally(e=>{
		//window.dispatchEvent(new Event('resize'));
		always(e);
	});
};*/

export default axios;
