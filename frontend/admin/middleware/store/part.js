import {Part} from '../api'

export default {
	namespaced: true,

	state: {
		list: [],

		item: {},

		default: {
			title: '',
			keywords: '',
			description: '',
			html: '',
		},

	},

	mutations: {
		SET_LIST(state, value){
			state.list = Array.isArray(value) ? value : [];
		},

		SET_ITEM(state, value){
			state.item = JSON.parse(JSON.stringify(Object.assign({}, state.default, value)));
		},

		SET_CURRENT(state, { name, value }) {
			state.item[name] = value;
		},
	},

	actions: {

		getAll({commit}){
			return Part.getAll()
				.then(({data})=>{
					commit("SET_LIST", data);
				})
			;
		},

		getItem({commit}, id){
			return Part.getItem(id)
				.then(({data})=>{
					commit("SET_ITEM", data);
				})
				;
		},

		setDefault({commit, state}){
			commit("SET_ITEM", state.default);
		},

		setCurrent({commit, state}, data) {
			commit("SET_CURRENT", data);
		},

		edit({state}){
			return Part.edit(state.item);
		},

	},

	getters: {


	}
}