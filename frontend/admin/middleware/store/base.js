import {Base} from '../api'

export default {
	namespaced: true,

	state: {
		sections: [],

		authorized: null,

		statistic: {},

		admin: null
	},

	mutations: {
		setLogStatus(state, value){
			state.authorized = value;
		},

		setCurrentAdmin(state, value){
			state.admin = value;
		},

		setStatistic(state, value){
			state.statistic = value;
		},

		logOut(state){
			state.authorized = false;
			state.admin = null;
		}
	},

	actions: {
		checkAuth({commit, dispatch}){
			return Base.checkAuth()
				.then(()=>{
					commit("setLogStatus", true);
					dispatch("getCurrentAdmin");
				})
				.catch(()=>{
					commit("setLogStatus", false);
				})
			;
		},

		logIn({commit, dispatch}, data){
			return Base.logIn(data)
				.then(()=>{
					commit("setLogStatus", true);
					dispatch("getCurrentAdmin");
				})
			;
		},

		logOut({commit}, data){
			return Base.logOut()
				.then(()=>{
					commit("logOut");
				})
			;
		},

		getCurrentAdmin({commit}){
			return Base.getCurrentAdmin()
				.then((data)=>{
					commit("setCurrentAdmin", data);
				})
			;
		},

		searchAddress(_, query){
			return Base.searchAddress(query);
		},

		clearCache(){
			return Base.clearCache();
		},

		getStatistic({commit}){
			return Base.getStatistic()
				.then(({data})=>{
					commit("setStatistic", data);
				})
			;
		}
	},

	getters: {

		getAdmin: state => (name) => {
			return state.admin && state.admin[name] || '';
		},

		getSections: state => (id) => {
			return state.sections.find((s)=> s.id === id);
		},

		isSuper: state => (name) => {
			return state.admin.super === '1';
		},
	}
}