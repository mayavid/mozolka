import {Section} from '../api'


export default {
	namespaced: true,

	state: {
		list: [],

		item: {},

		default: {
			parent: '',
			name: '',
			slug: '',
			id_page: '',
			rating: '',
			publish: true,
		},

	},

	mutations: {
		SET_LIST(state, value){
			state.list = Array.isArray(value) ? value : [];
		},

		SET_ITEM(state, value){
			state.item = JSON.parse(JSON.stringify(Object.assign({}, state.default, value)));
		},

		SET_CURRENT(state, { name, value }) {
			state.item[name] = value;
		},
	},

	actions: {

		getAll({commit}){
			return Section.getAll()
				.then(({data})=>{
					commit("SET_LIST", data);
				})
			;
		},

		getItem({commit}, id){
			return Section.getItem(id)
				.then(({data})=>{
					commit("SET_ITEM", data);
				})
			;
		},

		setDefault({commit, state}){
			commit("SET_ITEM", state.default);
		},

		setCurrent({commit, state}, data) {
			commit("SET_CURRENT", data);
		},

		add({state}){
			return Section.add(state.item);
		},

		edit({state}){
			return Section.edit(state.item);
		},

		delete(_, id){
			return Section.delete(id);
		},

	},

	getters: {


	}
}