import {Article as Api} from '../api'
import moment from 'moment'

export default {
	namespaced: true,

	state: {
		list: [],

		item: {},

		default: {
			title: '',
			keywords: '',
			description: '',
			id_articles_section: '',
			html: '',
			anchor: '',
			slug: '',
			added: moment().format('YYYY-MM-DD'),
			gallery: [],
			publish: true,
		},

	},

	mutations: {
		SET_LIST(state, value){
			state.list = Array.isArray(value) ? value : [];
		},

		SET_ITEM(state, value){
			state.item = JSON.parse(JSON.stringify(Object.assign({}, state.default, value)));
		},

		SET_CURRENT(state, { name, value }) {
			state.item[name] = value;
		},
	},

	actions: {

		getAll({commit}){
			return Api.getAll()
				.then(({data})=>{
					commit("SET_LIST", data);
				})
				;
		},

		getItem({commit}, id){
			return Api.getItem(id)
				.then(({data})=>{
					commit("SET_ITEM", data);
				})
				;
		},

		setDefault({commit, state}){
			commit("SET_ITEM", state.default);
		},

		setCurrent({commit, state}, data) {
			commit("SET_CURRENT", data);
		},

		add({state}){
			return Api.add(state.item);
		},

		edit({state}){
			return Api.edit(state.item);
		},

		delete(_, id){
			return Api.delete(id);
		},

	},

	getters: {


	}
}