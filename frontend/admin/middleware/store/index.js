
import Vue from 'vue'
import Vuex from 'vuex'
import base from './base'
import manager from './manager'
import page from './page'
import section from './section'
import news from './news'
import part from './part'
import article from './article'
import articles_section from './articles_section'
Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		base,
		manager,
		page,
		section,
		news,
		part,
		article,
		articles_section,
	}
})


