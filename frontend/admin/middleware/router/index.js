import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes'

Vue.use(Router);

const router = new Router({
	base: isLocal ? '/admin.html' : '/admin/',
	mode: isLocal ? 'hash' : 'history',
	routes,
	linkActiveClass: '',
	linkExactActiveClass: 'is-active',
	transitionOnLoad: true,
	root: '/'
});

router.afterEach((to, from) => {
	document.title = to.meta.title;
	window.scrollTo(0, 0);
});

export default router