import Home from '../../views/home'
import Form from '../../views/form'
import Settings from '../../views/settings'

import Manager from '../../views/manager'
import ManagerItem from '../../views/manager/item'

import Section from '../../views/section'
import SectionItem from '../../views/section/item'

import News from '../../views/news'
import NewsItem from '../../views/news/item'

import Article from '../../views/article'
import ArticleItem from '../../views/article/item'

import ArticlesSection from '../../views/articles_section'
import ArticlesSectionItem from '../../views/articles_section/item'

import Page from '../../views/page'
import PageItem from '../../views/page/item'

import Part from '../../views/part'
import PartItem from '../../views/part/item'


import Error from '../../views/error'



export default [
	{
		path: '/',
		component: Home,
		name: 'home',
		meta: {
			title: 'Главная'
		}
	},
	{
		path: '/c9760b84e2/in',
		redirect: {
			name: 'home'
		},
	},

	{
		path: '/manager',
		component: Manager,
		name: 'manager',
		meta: {
			title: 'Администраторы',
		},
	},
	{
		path: '/manager/add',
		component: ManagerItem,
		name: 'manager_add',
		meta: {
			title: 'Добавить администратора',
		},
	},
	{
		path: '/manager/:managerId',
		component: ManagerItem,
		name: 'manager_item',
		meta: {
			title: 'Администратор',
		},
	},

	{
		path: '/section',
		component: Section,
		name: 'section',
		meta: {
			title: 'Разделы',
		},
	},
	{
		path: '/section/add',
		component: SectionItem,
		name: 'section_add',
		meta: {
			title: 'Добавить раздел',
		},
	},
	{
		path: '/section/:sectionId',
		component: SectionItem,
		name: 'section_item',
		meta: {
			title: 'Раздел',
		},
	},

	// новости
	{
		path: '/news',
		component: News,
		name: 'news',
		meta: {
			title: 'Новости',
		},
	},
	{
		path: '/news/add',
		component: NewsItem,
		name: 'news_add',
		meta: {
			title: 'Добавить новость',
		},
	},
	{
		path: '/news/:newsId',
		component: NewsItem,
		name: 'news_item',
		meta: {
			title: 'Новость',
		},
	},

	// статьи
	{
		path: '/article',
		component: Article,
		name: 'article',
		meta: {
			title: 'Статьи',
		},
	},
	{
		path: '/article/add',
		component: ArticleItem,
		name: 'article_add',
		meta: {
			title: 'Добавить статью',
		},
	},
	{
		path: '/article/:articleId',
		component: ArticleItem,
		name: 'article_item',
		meta: {
			title: 'Статья',
		},
	},

	// разделы статей
	{
		path: '/articles_section',
		component: ArticlesSection,
		name: 'articles_section',
		meta: {
			title: 'Разделы статей',
		},
	},
	{
		path: '/articles_section/add',
		component: ArticlesSectionItem,
		name: 'articles_section_add',
		meta: {
			title: 'Добавить раздел статей',
		},
	},
	{
		path: '/articles_section/:articlesSectionId',
		component: ArticlesSectionItem,
		name: 'articles_section_item',
		meta: {
			title: 'Раздел статей',
		},
	},



	{
		path: '/page',
		component: Page,
		name: 'page',
		meta: {
			title: 'Страницы',
		},
	},
	{
		path: '/page/add',
		component: PageItem,
		name: 'page_add',
		meta: {
			title: 'Добавить страницу',
		},
	},
	{
		path: '/page/:pageId',
		component: PageItem,
		name: 'page_item',
		meta: {
			title: 'Страница',
		},
	},



	// фиксированные страницы
	{
		path: '/part',
		component: Part,
		name: 'part',
		meta: {
			title: 'Страницы',
		},
	},

	{
		path: '/part/:partId',
		component: PartItem,
		name: 'part_item',
		meta: {
			title: 'Страница',
		},
	},



	{
		path: '/settings',
		component: Settings,
		name: 'settings',
		meta: {
			title: 'Настройки',
			group: 'settings'
		},
	},


	{
		path: '/form',
		component: Form,
		meta: {
			title: 'Примеры полей формы',
			group: 'forms'
		}
	},



	{
		path: '*/*',
		component: Error,
		meta: {
			title: 'Страница не существует'
		}
	}
];

