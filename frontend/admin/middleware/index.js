import eventHub from './eventhub'
import store from './store'
import router from './router'
import filters from './filters'
import ripple from './ripple'
import scrollbar from './scrollbar'
import dragndrop from 'vue-drag-drop';
import tooltip from 'v-tooltip';
import notification from 'vue-toast-notification';
import lazyload from 'vue-lazyload'

const install = {
	eventHub: {
		plugin: {
			install: V => {
				V.prototype.$eventHub = eventHub;
			}
		},
		options: {}
	},
	ripple: {
		plugin: {
			install: (V)=>{
				V.directive('ripple', ripple);
			}
		},
		options: {}
	},

	notification: {
		plugin: notification,
		options: {}
	},
	scrollbar: {
		plugin: scrollbar,
		options: {}
	},
	dragndrop: {
		plugin: dragndrop,
		options: {}
	},
	tooltip: {
		plugin: tooltip,
		options: {}
	},
	lazyload: {
		plugin: lazyload,
		options: {
			preLoad: 1.3,
			error: '/assets/img/svg/btn-loader.svg',
			loading: '/assets/img/svg/btn-loader.svg',
			attempt: 1
		}
	},
};

export {
	store,
	router,
	filters,
	install
}
