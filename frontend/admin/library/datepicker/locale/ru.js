import ru from 'date-format-parse/lib/locale/ru';

const lang = {
  formatLocale: ru,
  yearFormat: 'YYYY',
  monthFormat: 'MMMM',
  monthBeforeYear: true,
};

export default lang;
